; VIDEO CONTROLLER PROGRAM FOR CHINA SWITCHERS BY DON LULHAM.
; COPYRIGHT 2013 - 2017, LULHAM LOGIC INC. FOR MIRROR VISION TECHNOLOGIES INC.
;
; OCTOBER 31. 2016, CHANGING PROCESSOR TO THE 16F77
; - DESIGNED TO WORK WITH THE MK-4 AND MK-5 20 BUTTON KEYPAD -
;
; NOVEMBER 28, 2016, BIG CHANGES IN HARDWARE. GAVE UP ON I2C AUDIO.
; NOVEMBER 28, 2016, CHANGING RELAYS FOR MAX313 FOR CAMERA SELECTS.
; DECEMBER 2,  2016, FIXED INTERRUPTS. FORGOT OPTION REG..!
; DECEMBER 2,  2016, WORKING ON AUDIO BUTTONS
; DECEMBER 5,  2016, CHANGING TO TIMER1 FOR KEY DELAY. TIMER0 FOR BEEP CLOCK
; DECEMBER 6,  2016, TIMER-1 IS WORKING. 
; DECEMBER 8,  2016, USING A SUBROUTINE FOR THE BEEP FUNCTION. TIMER-0 REMOVED.
; DECEMBER 18, 2016, FIXED REC MENU ERROR. ADDED BSF FLAGS,CAM_FLAG TO KEYS.
; DECEMBER 20, 2016, CORRECTED PWR_FLAG. 0 = OFF, 1 = ON
; DECEMBER 21, 2016, MAIN POWER IS NOW FROM A1. NOT THRU THE SWITCH. HARDWARE MOD..!
; DECEMBER 28, 2016, AUDIO VOL AND POWER UP CONTRL PROBLEMS. FIXED.
; DECEMBER 29, 2016, ADDED AUTO POWER UP DETECT.
; DECEMBER 29, 2016, ADDING LOCKOUTS FOR FLEET. (AUTO POWER UP)
; JANUARY 30,  2017, PROBLEMS WITH THE BLINKING AUTO/MANUAL SWITCH...!
; FEBRUARY 7,  2017, REMOVED AUTO/MANUAL CODE FOR NOW...
; FEBRUARY 8,  2017, PROGRAMMED NEW VIDEO BOARD. SYSTEM WORKED.
; FEBRUARY 12, 2017. CHANGED I/O PINS A0 = SHUTTER AND A4 = AUTO INPUT. CHANGED FILE NAME V3a
; FEBRUARY 12, 2017, CHANGED BUTTONS. KILL IS NOW BUTTON-2 AND MIRRORHEAD POWER IS BUTTON-3
; FEBRUARY 13, 2017, MODIFID MK5_V2e PCB TO MK5_V3a WITH NEW PORT ASSIGNMENTS.
; FEBRUARY 13, 2017, PROGRAMMED UNIT. NEW CODE WORKS. SHUTTERS FUNCTION PROPERLY.
; FEBRUARY 18, 2017, PROBLEM WITH SHUTTER. WHEN SYSTEM IS POWERED OFF, 
;					 SHUTTER IF ON WILL POWER THE HEATER AND IR LEDS. NEED TO FIX THIS IN SOFTWARE.
; FEBRUARY 19, 2017, ADDED SUTTER OFF IN POWER ON/OFF AND ADDED TEST FOR PWR ON/OFF TO SHUTTER.
;
; CHECKSUM = 0x4302
;--------------------------------------------------------------------------------------
; SEE END OF FILE FOR PROGRAMMING NOTES
;
; FILE NAME "VCONTROL_MK5_V3a.ASM"
	list p=16F77
	#INCLUDE <P16F77.INC>
	__config _XT_OSC & _WDT_OFF & _CP_OFF

;--------------------------------------------------------------------------------------
; DEFINE EQUATES:
;--------------------------------------------------------------------------------------

C 			EQU 0 				;   OR 	 	 XOR 	 	AND
DC 			EQU 1 				; ------ 	------ 		------
Z 			EQU 2 				; 00 / 0 	00 / 0 		00 / 0
RP1 		EQU 6 				; 01 / 1 	01 / 1 		01 / 0
RP0 		EQU 5 				; 10 / 1 	10 / 1 		10 / 0
F0 			EQU 0 				; 11 / 1 	11 / 0 		11 / 1
CARRY 		EQU 0 				;
F 			EQU 1 				;
MSB 		EQU 7 				;
STATUS		EQU 3

;--------------------------------------------------------------------------------------
; DEFINE RAM LOCATIONS:
;--------------------------------------------------------------------------------------

PORT_A 		EQU 5 				; 
PORT_B 		EQU 6 				;
PORT_C 		EQU 7 				;
PORT_D		EQU	8				;
PORT_E		EQU	9				;

W_TEMP		EQU	20H
STATUS_TEMP	EQU	21H
PCLATH_TEMP	EQU	22H

DCOUNT 		EQU 23H 			; COUNTER USED BY DELAY100
FLAGS 		EQU 24H				; MAIN CONTROL FLAGS
TEMP 		EQU 25H 			;
XMTREG 		EQU 26H 			; HOLDS KEYPAD DATA FOR SERIAL OUTPUT
RCVREG 		EQU 27H 			; HOLDS DATA RECIEVED BY RCVR SUBROUTINE
XCOUNT	 	EQU 28H 			; USED BY DAC ROUTINES
LED_DATA_X 	EQU 29H 			; USED BY FIND KEY ROUTINE
DAC_DATA 	EQU 2AH 			; DATA FOR THE DAC
BITCOUNT	EQU	2BH				; USED BY SERIAL OUT
TIME_FLAGS	EQU	2CH				;
CYCLES		EQU	2DH				; USED BY BEEP
T1COUNT		EQU	2EH				; USED BY INTERRUPT TIMER1
RCOUNT		EQU	2FH				; USED BY KEYDELAY TIMER

;--------------------------------------------------------------------------------------
; FLAGS REGISTER - STORED IN FLAGS REGISTER
;--------------------------------------------------------------------------------------
RX_FLAG 	EQU 0 				; 1=GOOD 0=BAD KEYPAD DATA
DN_FLAG 	EQU 1 				; DAY/NIGHT FLAG. 0=DAY 1=NIGHT. DEFAULT IS DAY=0
PWR_FLAG 	EQU 2 				; POWER O/F FLAG. 1=ON 0=OFF
FLEET_FLAG	EQU	3				; 0=NORMAL 1=AUTO POWER UP
CAM_FLAG	EQU 4				; DEFAULT MODE. CAMERAS SELECTED
AUDIO_FLAG	EQU	5				; 0=AUDIO OFF, 1=AUDIO ON
BEEP_FLAG	EQU	6				; BEEP_FLAG = 1 THEN BEEP

;--------------------------------------------------------------------------------------
; TIME_FLAGS REGISTER - STORED IN TIME_FLAGS REGISTER
;--------------------------------------------------------------------------------------
S_FLAG		EQU	0				; SWITCHER MENU TIME FLAG
R_FLAG		EQU	1				; RECORDER MENU TIME FLAG
V_FLAG		EQU 2				; VOLUME TIME FLAG
B_FLAG		EQU 3				; BRIGHTNESS TIME FLAG

;--------------------------------------------------------------------------------------
; PROGRAM CONSTANTS
;--------------------------------------------------------------------------------------
N_VALUE 	EQU 0xAA 			; DEFAULT VALUE FOR NIGHT BRIGHTNESS
D_VALUE 	EQU 0xFF 			; DEFAULT VALUE FOR DAY BRIGHTNESS
INC_VALUE 	EQU 0x10 			; DEFAULT FOR INCREMENT
DEC_VALUE 	EQU 0x10 			; DEFAULT FOR DECREMENT
T1_VALUE	EQU	0x08			; DELAY FOR BRIGHTNESS AND VOLUME CONTROLS 
T2_VALUE	EQU	0x0A			; DELAY FOR SWITCHER AND RECORDER MENUS

;--------------------------------------------------------------------------------------
; I/O PORTS
;--------------------------------------------------------------------------------------

SHUT		EQU 0				; PORT-A BIT-0 - OUTPUT - 2N3904
PWR 		EQU 1 				; PORT-A BIT-1 - OUTPUT - 2N3904
MIRROR		EQU	2				; PORT_A BIT-2 - OUTPUT - 2N3904
HEAT 		EQU 3 				; PORT-A BIT-3 - OUTPUT - 2N3904
AUTO 		EQU 4 				; PORT-A BIT-4 - INPUT
KILL 		EQU 5 				; PORT-A BIT-5 - OUTPUT - DIGITAL 0=OFF 1=ON

UP			EQU	0				; PORT-B BIT-0 - OUTPUT - IN-1(U9) - MAX313
DOWN		EQU 1				; PORT-B,BIT-1 - OUTPUT - IN-2(U9) - MAX313
LEFT		EQU 2				; PORT-B BIT-2 - OUTPUT - IN-3(U9) - MAX313
RIGHT		EQU 3				; PORT-B BIT-3 - OUTPUT - IN-4(U9) - MAX313
SPLIT 		EQU 4 				; PORT-B BIT-4 - OUTPUT - IN-4(U6) - MAX313
BEEP_SW		EQU 5				; PORT-B BIT-5 - OUTPUT - IN-1(U3) - MAX313
AUDIO_SW	EQU 6				; PORT-B BIT-6 - OUTPUT - IN-2(U3) - MAX313 
INT_DELAY	EQU 7				; PORT-B BIT-7 - OUTPUT - TO A LED. SHOWS INTERRUT TIME.

DAC_CS 		EQU 0 				; PORT-C BIT-0 - OUTPUT - DIGITAL 0=ON 1=OFF - MCP4801
DAC_SCK 	EQU 1 				; PORT-C BIT-1 - OUTPUT - DIGITAL I/O - MCP4801
DAC_SDI 	EQU 2 				; PORT-C BIT-2 - OUTPUT - DIGITAL I/O - MCP4801
DAC_LOAD	EQU 3				; PORT-C BIT-3 - OUTPUT - DIGITAL I/O - MCP4801
OP_CS		EQU 4				; PORT-C BIT-4 - OUTPUT - DIGITAL 0=ON 1=OFF - MCP603
VOL_CLK		EQU 5  				; PORT-C BIT-5 - OUTPUT - NO-1(U2) - MAX313 
MODE_STBY	EQU 6				; PORT-C BIT-6 - OUTPUT - IN-2(U2) - MAX313 
VOL_SW		EQU 7				; PORT-C BIT-7 - OUTPUT - IN-1(U2) - MAX313 

REC_MENU	EQU 0				; PORT-D BIT-0 - OUTPUT - IN-3(U3) - MAX313
RECORD		EQU 1				; PORT-D BIT-1 - OUTPUT - IN-4(U3) - MAX313
REC_STOP	EQU 2				; PORT-D BIT-2 - OUTPUT - IN-1(U6) - MAX313
REC_UP		EQU 3				; PORT-D BIT-3 - OUTPUT - IN-2(U6) - MAX313 
REC_DOWN	EQU 4				; PORT-D BIT-4 - OUTPUT - IN-3(U6) - MAX313
MODE_MUTE	EQU 5				; PORT-D BIT-5 - OUTPUT - IN-3(U2) - MAX313  
MODE_RUN	EQU 6				; PORT-D BIT-6 - OUTPUT - IN-4(U2) - MAX313 
BEEP_SIG	EQU 7				; PORT-D BIT-7 - OUTPUT - NO-1(U3) - MAX313 

RX 			EQU 0 				; PORT-E BIT-0 - INPUT  - DIGITAL I/O - (SIN)
DX 			EQU 1 				; PORT-E BIT-1 - OUTPUT - DIGITAL I/O - (SOUT)
SERIAL_OUT	EQU	2				; PORT-E BIT-2 - OUTPUT - DIGITAL I/O - SERIAL OUT TO SWITCHER KEYPAD INPUT

;--------------------------------------------------------------------------------------
	ORG 0000 					; RESET ADDRESS
;--------------------------------------------------------------------------------------

RESET
	CALL 	INITS_SYSTEM 		; INITIALIZE SYSTEM
;	CALL	AUTO_DETECT
	GOTO	MAIN_LOOP

;--------------------------------------------------------------------------------------
	ORG 0004					; INTERUPT ADDRESS
;--------------------------------------------------------------------------------------

	GOTO	ISR_TIMERS			; TEST FOR TIMEOUT

;--------------------------------------------------------------------------------------
	ORG 0010					; START OF PROGRAM
;--------------------------------------------------------------------------------------

;======================================================================================
; INITS_SYSTEM SUBROUTINE
;======================================================================================
INITS_SYSTEM
	BCF		STATUS,RP1
	BCF		STATUS,RP0			; PAGE-0 RAM

	CLRF	PORT_A
	BSF		STATUS,RP0			; PAGE-1
	MOVLW 	0x07		 		; MAKE ALL PINS DIGITAL
	MOVWF 	ADCON1

	BCF		STATUS,RP0			; PAGE-0
	CLRF	PORT_A

	BSF		STATUS,RP0			; PAGE-1
	MOVLW	B'00010000'			; A0 TO A3 = OUTPUTS. A4 = INPUT, A5 = OUTPUT
	MOVWF	TRISA				; PROGRAM PORT		

	BCF		STATUS,RP0			; PAGE-0
	CLRF	PORT_B
	
	BSF		STATUS,RP0			; PAGE-1
	MOVLW 	B'00000000' 		; SET PORT B AS OUTPUTS
	MOVWF	TRISB

	BCF		STATUS,RP0			; PAGE-0
	CLRF	PORT_C

	BSF		STATUS,RP0			; PAGE-1
	MOVLW 	B'00000000' 		; SET PORT C AS OUTPUTS
	MOVWF	TRISC

	BCF		STATUS,RP0			; PAGE-0
	CLRF	PORT_D

	BSF		STATUS,RP0			; PAGE-1
	MOVLW 	B'00000000' 		; SET PORT D AS OUTPUTS
	MOVWF	TRISD

	BCF		STATUS,RP0			; PAGE-0
	CLRF	PORT_E

	BSF		STATUS,RP0			; PAGE-1
	MOVLW 	B'00000001' 		; E0 = INPUT, E1, E2 = OUTPUTS
	MOVWF	TRISE

	BCF		STATUS,RP0			; PAGE-0

	MOVLW	B'00110101'			; CLOCK = OSC/4, PRESCALER = 8
	MOVWF	T1CON				; TIMER-1 SETUP

	BSF		INTCON,GIE			; ENABLE INTERRUTS
	NOP
	BSF		INTCON,PEIE			; SET PERIPHERAL. THIS IS CRITICAL..!
	NOP
	
; CLEAR REGISTERS AND PORTS -----------------------------------------------------------

	CLRF	PORT_A
	CLRF	PORT_B
	CLRF 	PORT_C
	CLRF	PORT_D
	CLRF	PORT_E

	CLRF	TMR1L
	CLRF	TMR1H
	CLRF	PIR1	

	CLRF 	FLAGS 
	CLRF	TIME_FLAGS
	CLRF 	RCVREG
	CLRF 	TEMP

; INITS REGISTERS AND PORTS -----------------------------------------------------------

	BSF 	PORT_E,DX 			; DX, SERIAL OUT HIGH
	NOP
	BCF		PORT_A,HEAT			; TURN HEATERS OFF
	NOP	
	BCF 	PORT_A,PWR 			; POWER OFF ON POWER UP
	NOP
	BSF		PORT_E,SERIAL_OUT	; SERIAL OUT TO SWITCHER HI.
	NOP
	BSF 	PORT_C,DAC_LOAD		; DISABLE DAC
	NOP
	BSF 	PORT_C,DAC_CS		; DISABLE DAC
	NOP
	BSF 	PORT_C,OP_CS 		; OPAMP OFF
	NOP
	BCF 	FLAGS,PWR_FLAG 		; PWR IS OFF.  0 = OFF 1 = ON..
	NOP
	BSF		FLAGS,CAM_FLAG		; DEFAULT SELECT CAMERAS.
	NOP	
	BCF		FLAGS,FLEET_FLAG	; CLEAR AUTO FLAG
	NOP


; ADJUST BEEP VOLUME (AUDIO TOO) TO A LOADER SETTING ----------------------------------

	MOVLW	0x15				; 19HEX WAS A BIT LOAD. TRYING 15HEX (21 PUSHES ON VOL CONTRL)
	MOVWF	TEMP

VOL_HIGHER
	BSF		PORT_C,VOL_CLK
	NOP
	BSF		PORT_C,VOL_SW		; CONNECT VOL CLK TO UP/DN PIN
	NOP
	NOP
	BCF		PORT_C,VOL_SW		; VOL TO FLOAT
	NOP

	DECFSZ	TEMP,F
	GOTO	VOL_HIGHER

	BSF		PORT_C,MODE_STBY	; DISABLE AUDIO
	NOP

; INITS THE DAC FOR DEFAULT ON DAY BRIGHTNESS -----------------------------------------

	MOVLW 	D_VALUE
	MOVWF 	DAC_DATA
	CALL 	WR_DAC
	BSF 	FLAGS,DN_FLAG 		; OPPOSITE SO NEXT KP WILL GO NIGHT
	RETLW	0

;======================================================================================
; AUTO_DETECT SUBROUTINE - CALLED ON POWER UP.
;======================================================================================

; TEST AUTO POWER UP SLIDE SWITCH -----------------------------------------------------

;AUTO_DETECT
;	BTFSS	PORT_A,AUTO			; 1=NORMAL, 0= AUTO. (CURRENTLY BACKWARDS)
;	GOTO	AUTO_PWR_UP

; NORMAL POWER UP ---------------------------------------------------------------------

;	BCF 	FLAGS,PWR_FLAG 		; PWR IS OFF.  0 = OFF 1 = ON
;	NOP							; WAIT FOR POWER BUTTON EE SIGNAL
;	BCF		FLAGS,FLEET_FLAG		; CLEAR AUTO FLAG
;	NOP
;	RETLW	0

; AUTO PWR UP SO TURN THINGS ON AND LOCK OUT STOP RECORDING. --------------------------

;AUTO_PWR_UP
;	CALL	INITS_SYSTEM		; PWR WAS OFF, NOW TURN IT ON
;	NOP
;	BSF 	PORT_A,PWR 			; PWR IS NOW ON
;	NOP
;	BSF 	PORT_A,MIRROR 		; MIRROR IS NOW ON
;	NOP
;	BCF 	PORT_C,OP_CS 		; OPAMP ON
;	NOP
;	BCF		PORT_C,DAC_CS		; DAC IS ON
;	NOP
;	BSF 	PORT_A,KILL 		; KILL ON (+5V)
;	NOP
;	BSF		PORT_B,DOWN			; SELECT BACK CAMERA.
;	NOP
;	BSF 	FLAGS,PWR_FLAG 		; PWR_FLAG = 1 = ON
;	NOP
;	BSF 	FLAGS,CAM_FLAG 		; CAM_FLAG = 1 = ON
;	NOP
;	BSF 	FLAGS,FLEET_FLAG 	; FLEET_FLAG = 1 = ON
;	NOP
;	CALL	BEEP
;	RETLW	0

;======================================================================================
; DELAY25 SUBROUTINE - USED BY WR_DAC
;======================================================================================
DELAY25
	MOVLW 	25
	MOVWF 	DCOUNT
DLOOP6
	DECFSZ 	DCOUNT,F
	GOTO 	DLOOP6
	RETLW 	0

;======================================================================================
; DELAY50 SUBROUTINE PAGE-0 - USED BY RCVR ROUTINE
;======================================================================================
DELAY50
	MOVLW 	50
	MOVWF 	DCOUNT
DLOOP2
	DECFSZ 	DCOUNT,F
	GOTO 	DLOOP2
	RETLW 	0

;======================================================================================
; DELAY75 SUBROUTINE - USED BY RCVR ROUTINE
;======================================================================================
DELAY75
	MOVLW 	75
	MOVWF 	DCOUNT
DLOOP1
	DECFSZ 	DCOUNT,F
	GOTO 	DLOOP1
	RETLW 	0

;======================================================================================
; KEYDELAY SUBROUTINE - USED BY RECORDER	
;======================================================================================
KEYDELAY
	MOVLW 	0x80				; MEASUERED 1.1 SECOND DELAY AT 80 HEX
	MOVWF 	RCOUNT
RELOAD_KEY
	MOVLW 	0xFF
	MOVWF 	DCOUNT
LOOPIN
	DECFSZ 	DCOUNT,F
	GOTO 	LOOPIN

	DECFSZ	RCOUNT,F
	GOTO	RELOAD_KEY

	RETLW 	0

;======================================================================================
; BEEP SUBROUTINE - 	
;======================================================================================
BEEP
	BSF		PORT_B, BEEP_SW
	NOP
	BCF		PORT_B,AUDIO_SW
	NOP
	BCF		PORT_C,MODE_STBY
	NOP
	BSF		PORT_D,MODE_RUN
	NOP	
	MOVLW	0xFF
	MOVWF	CYCLES

LOAD_LOOP
	MOVLW 	0x02			
	MOVWF 	RCOUNT
RELOAD_BEEP
	MOVLW 	0xFF
	MOVWF 	DCOUNT
LOOP1
	DECFSZ 	DCOUNT,F
	GOTO 	LOOP1

	DECFSZ	RCOUNT,F
	GOTO	RELOAD_BEEP

	BTFSS	PORT_D,BEEP_SIG
	GOTO	WAS_LOW

	BCF		PORT_D,BEEP_SIG
	GOTO	DEC_CYCLES

WAS_LOW
	BSF		PORT_D,BEEP_SIG

DEC_CYCLES
	DECFSZ	CYCLES,F
	GOTO	LOAD_LOOP

; TEST IF AUDIO WAS ON ----------------------------------------------------------------

	BTFSS	FLAGS,AUDIO_FLAG		; WAS AUDIO ON..? 1=ON
	GOTO	AUDIO_ON

; AUDIO WAS OFF -----------------------------------------------------------------------

	BCF		PORT_B, BEEP_SW
	NOP
	BCF		PORT_B,AUDIO_SW
	NOP
	BCF		PORT_C,MODE_STBY
	NOP
	BSF		PORT_D,MODE_RUN
	NOP		
	RETLW 	0

AUDIO_ON
	BCF		PORT_B, BEEP_SW
	NOP
	BSF		PORT_B,AUDIO_SW
	NOP
	BCF		PORT_C,MODE_STBY
	NOP
	BSF		PORT_D,MODE_RUN
	NOP		
	RETLW 	0

;======================================================================================
; TIMER_TEST - ISR SUBROUTINE - TRIGGERED BY TMR1IF (TIMER1)
;			 - TESTS T1COUNT TO SEE IF 4 SECS HAVE PASSED 
;			 - OTHERWISE IT JUST DECS THE VALUE IN T1COUNT AND THEN EXITS
;			 - IF 0 THEN IT CLRS TIME_FLAGS - DISABLES INTERRUPTS THEN EXITS
; 			 - APPLICATION LOADS THE TIMER AND ALLOWS INTERRUPTS.
;======================================================================================
ISR_TIMERS

; SAVE REGISTERS....

	MOVWF 	W_TEMP 				;Copy W to TEMP register
	SWAPF 	STATUS,W 			;Swap status to be saved into W
	CLRF 	STATUS 				;bank 0, regardless of current bank, Clears IRP,RP1,RP0
	MOVWF 	STATUS_TEMP 		;Save status to bank zero STATUS_TEMP register
	MOVF 	PCLATH, W 			;Only required if using pages 1, 2 and/or 3
	MOVWF 	PCLATH_TEMP 		;Save PCLATH into W
	CLRF 	PCLATH 				;Page zero, regardless of current page

; ISR CODE....TIMER-1 -----------------------------------------------------------------

ISR_KEYDELAY
	DECFSZ	T1COUNT,F			; DEC TCOUNT AND TEST FOR 0
	GOTO	RESET_TIME			; NOT 0 SO KEEP RUNNING. JUMP TO RESET_TIME

; TIMER-1 TIMEDOUT..-------------------------------------------------------------------

	BCF		PORT_B,7			; TEST.....!
	CLRF	TIME_FLAGS			; ITS 0 SO TIMED OUT. CLEAR FLAGS AND INTERRUPTS
	NOP
	BSF		FLAGS,CAM_FLAG		; set default, cameras
	NOP
	BSF		STATUS,RP0			; BANK-1
	BCF		PIE1,TMR1IE			; DISABLE INTERRUPTS
	NOP
	BCF		STATUS,RP0			; BANK-0
	BCF		PIR1,TMR1IF			; CLEAR INT FLAG
	BCF		T1CON,TMR1ON
	NOP
	GOTO	ISR_EXIT

; ISR CODE... TIMER-1 STILL RUNNING..DO NOT RESET T1COUNT.-----------------------------

RESET_TIME
	BSF		PORT_B,7			; TEST......!
	BSF		STATUS,RP0			; BANK-1
	BCF		PIE1,TMR1IE			; DISABLE INTERRUPTS
	NOP
	BCF		STATUS,RP0			; BANK-0
	BCF		PIR1,TMR1IF			; CLEAR INT FLAG
	NOP
	CLRF	TMR1L
	NOP
	CLRF	TMR1H
	NOP
	MOVLW	B'00110101'			; CLOCK = OSC/4, PRESCALER = 8
	MOVWF	T1CON				; TIMER1 SETUP

	BSF		STATUS,RP0			; BANK-1
	BSF		PIE1,TMR1IE			; ENABLE INTERRUPTS
	NOP							; TMR1 WILL INTERRUPT AGAIN.
	BCF		STATUS,RP0			; BANK-0
	BSF		T1CON,TMR1ON
	GOTO	ISR_EXIT

; RESTORE REGISTERS -------------------------------------------------------------------

ISR_EXIT
	MOVF 	PCLATH_TEMP,W 		;Restore PCLATH
	MOVWF 	PCLATH 				;Move W into PCLATH
	SWAPF 	STATUS_TEMP,W 		;Swap STATUS_TEMP register into W
	MOVWF 	STATUS 				;Move W into STATUS register
	SWAPF 	W_TEMP,F 			;Swap W_TEMP
	SWAPF 	W_TEMP,W 			;Swap W_TEMP into W

	RETFIE

;======================================================================================
; MAIN_LOOP ROUTINE 
;======================================================================================
MAIN_LOOP 						; THIS IS THE MAIN PROGRAM LOOP...!
	NOP
	BCF 	FLAGS,RX_FLAG
	NOP
	GOTO 	RCVR

R_RETURN 
	NOP
	NOP
	BTFSS 	FLAGS,RX_FLAG 		; IF SET THEN SKIP TO KEY ID
	GOTO 	BAD_DATA
	GOTO 	ID_KEY

BAD_DATA						; REALLY NEED TO PUT SOMETHING HERE
	GOTO 	MAIN_LOOP

;======================================================================================
; RCVR SERIAL_IN ROUTINE 
;======================================================================================
RCVR
	BCF 	FLAGS,RX_FLAG		
	NOP
	BTFSC 	PORT_E,RX
	GOTO 	RCVR				; WAITS FOR START PULSE....
	CLRF 	RCVREG 				; START BIT DETECTED
	CALL 	DELAY75
	MOVLW 	0x08
	MOVWF 	TEMP
R_NEXT
	BCF 	STATUS,CARRY
	RRF 	RCVREG,F
	BTFSC 	PORT_E,RX
	BSF 	RCVREG,MSB 			; DATA WAS A ONE
	NOP
	CALL 	DELAY50
	DECFSZ 	TEMP,F
	GOTO 	R_NEXT 				; MORE BITS TO COME
	NOP
	BTFSC 	PORT_E,RX 			; DETECTING A VALID STOP BIT
	GOTO 	RXBAD
	CALL 	DELAY50 			; RX IS LO
	NOP
	BTFSS 	PORT_E,RX
	GOTO 	RXBAD 				; STOP BIT WAS BAD

; VALID STOP BIT DETECTED, SO SEND AN ACK SIGNAL ON DX

	BCF 	PORT_E,DX
	CALL 	DELAY50
	BSF 	PORT_E,DX
	NOP
	BSF 	FLAGS,RX_FLAG 		; RX IS HI..STOP BIT DETECTED
	NOP
	GOTO 	R_RETURN
RXBAD
	BCF 	FLAGS,RX_FLAG
	NOP
	GOTO 	R_RETURN

;======================================================================================
; WR_DAC SUBROUTINE 
;
; ON ENTRY "DAC_DATA" HOLDS NUMBER TO SEND TO THE DAC.
;======================================================================================
WR_DAC
	BCF 	PORT_C,DAC_CS 		; CS =0
	MOVLW 	4
	MOVWF 	XCOUNT
	MOVLW 	B'00110000' 		; DAC CONTROL NIBBLE
	MOVWF 	TEMP
	BCF 	PORT_C,DAC_SCK 		; SCK=0
	CALL 	DELAY25

WR_CNTL
	BCF 	STATUS,CARRY
	RLF 	TEMP,F
	BTFSC 	STATUS,CARRY
	BSF 	PORT_C,DAC_SDI 		; DATA WAS A 1
	BTFSS 	STATUS,CARRY
	BCF 	PORT_C,DAC_SDI 		; DATA WAS A 0
	CALL 	DELAY25
	BSF 	PORT_C,DAC_SCK 		; CLK=1 LATCH DATA
	CALL 	DELAY25
	BCF 	PORT_C,DAC_SCK 		; CLK=0
	DECFSZ 	XCOUNT,F
	GOTO 	WR_CNTL

; DAC CONTROL BITS ARE SENT. NOW SEND 8 BITS OF DATA FROM DAC_DATA REG

	MOVLW 	8
	MOVWF 	XCOUNT
	MOVFW 	DAC_DATA 			; DAC VOLTAGE VALUE
	MOVWF 	TEMP
	BCF 	PORT_C,DAC_SCK 		; SCK=0
	CALL 	DELAY25

WR_VALUE
	BCF 	STATUS,CARRY
	RLF 	TEMP,F
	BTFSC 	STATUS,CARRY
	BSF 	PORT_C,DAC_SDI 		; DATA WAS A 1
	BTFSS 	STATUS,CARRY
	BCF 	PORT_C,DAC_SDI 		; DATA WAS A 0
	CALL 	DELAY25
	BSF 	PORT_C,DAC_SCK 		; CLK=1 LATCH DATA
	CALL 	DELAY25
	BCF 	PORT_C,DAC_SCK 		; CLK=0
	DECFSZ	XCOUNT,F
	GOTO 	WR_VALUE

; DAC VOLTAGE VALUE IS SENT. NOW DO 4 ZEROS..

	MOVLW 	4
	MOVWF 	XCOUNT
	MOVLW 	B'00000000' 		; DAC END NIBBLE
	MOVWF 	TEMP
	BCF 	PORT_C,DAC_SCK 		; SCK=0
	CALL 	DELAY25

WR_END
	BCF 	STATUS,CARRY
	RLF 	TEMP,F
	BTFSC 	STATUS,CARRY
	BSF 	PORT_C,DAC_SDI 		; DATA WAS A 1
	BTFSS 	STATUS,CARRY
	BCF 	PORT_C,DAC_SDI 		; DATA WAS A 0
	CALL 	DELAY25
	BSF 	PORT_C,DAC_SCK 		; CLK=1 LATCH DATA
	CALL 	DELAY25
	BCF 	PORT_C,DAC_SCK 		; CLK=0
	DECFSZ 	XCOUNT,F
	GOTO 	WR_END
	BSF 	PORT_C,DAC_CS 		; CS =1
	BCF 	PORT_C,DAC_LOAD 	; LOAD =0
	NOP
	NOP
	BSF 	PORT_C,DAC_LOAD 	; LOAD =1
	RETLW 	0

;======================================================================================
; ID_KEY ROUTINE 
;======================================================================================
ID_KEY
	MOVFW 	RCVREG 				; FIND OUT WHICH KEYPAD ROW
	ANDLW 	B'11110000' 		; MASK LO NIBBLE TO FIND ROW
	MOVWF 	LED_DATA_X 			; SAVE RESULT
	MOVLW 	0xE0
	SUBWF 	LED_DATA_X,W
	BTFSC 	STATUS,Z
	GOTO 	LED1 				; KEYPAD ROW 1

	MOVLW 	0xD0
	SUBWF 	LED_DATA_X,W
	BTFSC 	STATUS,Z
	GOTO 	LED2 				; KEYPAD ROW 2

	MOVLW 	0xB0
	SUBWF 	LED_DATA_X,W
	BTFSC 	STATUS,Z
	GOTO 	LED4 				; KEYPAD ROW 4

	MOVLW 	0x70
	SUBWF 	LED_DATA_X,W
	BTFSC 	STATUS,Z
	GOTO 	LED8 				; KEYPAD ROW 8

	GOTO 	MAIN_LOOP			; ERROR...!
LED1
	MOVFW 	RCVREG
	ANDLW 	B'00001111' 		; MASK HI NIBBLE FIND OUT WHICH COLUMN
	MOVWF 	TEMP 				; SAVE RESULT
	MOVLW 	0x0E
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_EE 				; PWR

	MOVLW 	0x0D
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_ED 				; MIRROR

	MOVLW 	0x0B
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_EB 				; KILL

	MOVLW 	0x07
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_E7 				; SHUTTERS

	MOVLW 	0x0F
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_EF 				; UP ARROW
	
	GOTO 	MAIN_LOOP
LED2
	MOVFW 	RCVREG
	ANDLW 	B'00001111' 		; MASK HI NIBBLE FIND OUT WHICH COLUMN
	MOVWF 	TEMP 				; SAVE RESULT
	MOVLW 	0x0E
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_DE 				;BRIGHTNESS
 
	MOVLW 	0x0D
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_DD 				; LEFT ARROW

	MOVLW 	0x0B
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_DB 				; SPLIT

	MOVLW 	0x07
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_D7 				; RIGHT ARROW

	MOVLW 	0x0F
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_DF 				; HEATER

	GOTO 	MAIN_LOOP
LED4
	MOVFW 	RCVREG
	ANDLW 	B'00001111' 		; MASK HI NIBBLE FIND OUT WHICH COLUMN
	MOVWF 	TEMP 				; SAVE RESULT
	MOVLW 	0x0E
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_BE 				; DOWN ARROW

	MOVLW 	0x0D
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_BD 				; SW MENU

	MOVLW 	0x0B
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_BB 				; AV

	MOVLW 	0x07
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_B7 				; QUAD

	MOVLW 	0x0F
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_BF 				; SW SEL/JYMP

	GOTO 	MAIN_LOOP
LED8
	MOVFW 	RCVREG
	ANDLW 	B'00001111' 		; MASK HI NIBBLE FIND OUT WHICH COLUMN
	MOVWF 	TEMP 				; SAVE RESULT
	MOVLW 	0x0E
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_7E 				; REC MENU

	MOVLW 	0x0D
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_7D 				; RECORD

	MOVLW 	0x0B
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_7B 				; STOP

	MOVLW 	0x07
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_77 				; AUDIO ON/OFF

	MOVLW 	0x0F
	SUBWF 	TEMP,W
	BTFSC 	STATUS,Z
	GOTO 	KEY_7F 				; AUDIO VOLUME

	GOTO 	MAIN_LOOP

;======================================================================================
; PWR KEY - CODE EE - TOGGLE ROUTINE
;======================================================================================
KEY_EE

;	BTFSS	FLAGS,FLEET_FLAG	; FLEET FLAG SET..?
;	GOTO	TEST_PWR
;	GOTO 	MAIN_LOOP			; FLEET FLAG IS SET SO IGNORE THIS KEY

;TEST_PWR
	MOVFW 	PORT_A
	MOVWF 	TEMP
	BTFSS 	TEMP,PWR			; 0 = OFF 1 = ON	
	GOTO 	PWR_WAS_OFF			; POWER IS OFF

; POWER IS CURRENTLY ON.....SO TURN IT OFF...

PWR_WAS_ON
	BCF 	PORT_A,PWR 			; PWR WAS ON, NOW TURN IT OFF
	BSF 	PORT_C,OP_CS 		; OPAMP OFF
	BCF 	FLAGS,PWR_FLAG 		; PWR_FLAG = 0 = OFF
	NOP
	BCF 	PORT_A,KILL 		; KILL OFF (0V)
	NOP
	BCF		PORT_C,DAC_SCK
	NOP
	BCF		PORT_C,DAC_LOAD
	NOP
	BSF		PORT_C,DAC_CS		; DAC IS OFF
	NOP
	BSF 	PORT_C,OP_CS 		; OPAMP OFF
	NOP
	BCF 	PORT_A,MIRROR 		; MIRROR IS NOW OFF
	NOP
	BCF 	PORT_A,SHUT 		; SHUTTER IS NOW OFF
	NOP
	BCF		FLAGS,AUDIO_FLAG
	NOP
	BCF		PORT_B, BEEP_SW
	NOP
	BCF		PORT_B,AUDIO_SW
	NOP
	BSF		PORT_C,MODE_STBY
	NOP
	BCF		PORT_D,MODE_RUN
	NOP
	
	MOVLW 	D_VALUE
	MOVWF 	DAC_DATA
	CALL 	WR_DAC
	BCF 	FLAGS,DN_FLAG
	CALL	BEEP
	GOTO 	MAIN_LOOP

; POWER IS CURRENTLY OFF....SO TURN IT ON...

PWR_WAS_OFF			
	CALL	INITS_SYSTEM		; PWR WAS OFF,, NOW TURN IT ON
	NOP
	BSF 	PORT_A,PWR 			; PWR IS NOW ON
	NOP
	BCF 	PORT_C,OP_CS 		; OPAMP ON
	NOP
	BCF		PORT_C,DAC_CS		; DAC IS ON
	NOP
	BSF 	PORT_A,KILL 		; KILL ON (+5V)
	NOP
	BSF		PORT_B,DOWN			; SELECT BACK CAMERA.
	NOP
	BSF 	FLAGS,PWR_FLAG 		; PWR_FLAG = 1 = ON
	NOP
	BSF 	FLAGS,CAM_FLAG 		; CAM_FLAG = 1 = ON
	NOP
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; MIRROR ON/OFF - CODE ED - TOGGLE ROUTINE
;======================================================================================
KEY_EB
	BTFSS 	FLAGS,PWR_FLAG
	GOTO 	MAIN_LOOP

	BSF		FLAGS,CAM_FLAG		; DEFAULT SELECT CAMERAS.
	NOP
	MOVFW 	PORT_A
	MOVWF 	TEMP
	BTFSS 	TEMP,MIRROR
	GOTO 	MIRROR_WAS_OFF
	BCF 	PORT_A,MIRROR 		; MIRROR IS NOW OFF
	CALL	BEEP
	GOTO 	MAIN_LOOP

MIRROR_WAS_OFF
	BSF 	PORT_A,MIRROR 		; MIRROR IS NOW ON
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; KILL KEY - CODE EB - TOGGLE ROUTINE
;======================================================================================
KEY_ED
	BTFSS 	FLAGS,PWR_FLAG
	GOTO 	MAIN_LOOP

	BSF		FLAGS,CAM_FLAG		; DEFAULT SELECT CAMERAS.
	NOP
	MOVFW 	PORT_A
	MOVWF 	TEMP
	BTFSS 	TEMP,KILL
	GOTO 	KILL_WAS_OFF

	BCF 	PORT_A,KILL 		; KILL IS NOW OFF
	NOP
	CALL	BEEP
	GOTO 	MAIN_LOOP

KILL_WAS_OFF
	BSF 	PORT_A,KILL 		; KILL IS NOW ON
	NOP
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; SHUT KEY - CODE E7 - TOGGLE ROUTINE
;======================================================================================
KEY_E7
	BTFSS 	FLAGS,PWR_FLAG
	GOTO 	MAIN_LOOP

	BSF		FLAGS,CAM_FLAG		; DEFAULT SELECT CAMERAS.
	NOP

	MOVFW 	PORT_A
	MOVWF 	TEMP
	BTFSS 	TEMP,SHUT
	GOTO 	SHUT_WAS_OFF

; SHUTTER WAS ON...

	BCF 	PORT_A,SHUT 		; SHUT IS NOW OFF
	NOP
	CALL	BEEP
	GOTO 	MAIN_LOOP

SHUT_WAS_OFF
	BSF 	PORT_A,SHUT 		; SHUT IS NOW ON
	NOP
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; UP ARROW KEY - CODE EF
;			   - S_FLAG SENDS SERIAL DATA TO SWITCHER FOR MENU SELECTIONS
;			   - R_FLAG OPENS AND CLOSES A RELAY
;			   - B_FLAG CALLS DIM+/- SUBROUTINES
;		   	   - THE FLAGS ABOVE RESETS THE TCOUNT AS A BUTTON WAS PUSHED.
;			   - CAM_FLAG DEFAULTS TO CAMERAS
;			   
;======================================================================================
KEY_EF
	BTFSS 	FLAGS,PWR_FLAG
	GOTO 	MAIN_LOOP

	BTFSC	FLAGS,CAM_FLAG
	GOTO	FRONT_CAM

	BTFSC	TIME_FLAGS,S_FLAG
	GOTO	SENDSERIAL_U

	BTFSC	TIME_FLAGS,R_FLAG
	GOTO	RECKEY_UP

	BTFSC	TIME_FLAGS,B_FLAG
	GOTO	BRIGHT_UP

	BTFSC	TIME_FLAGS,V_FLAG
	GOTO	VOLUME_UP

; CAMERA MODE -------------------------------------------------------------------------

FRONT_CAM
	MOVFW 	PORT_B
	MOVWF 	TEMP
	BTFSS 	TEMP,UP
	GOTO 	UP_OFF

	BCF 	PORT_B,UP 			; UP IS NOW OFF
	NOP
	CALL	BEEP
	GOTO 	MAIN_LOOP

UP_OFF
	BSF 	PORT_B,UP	 		; UP IS NOW ON
	NOP
	BCF		PORT_B,DOWN
	NOP
	BCF		PORT_B,LEFT
	NOP
	BCF		PORT_B,RIGHT
	NOP
	BCF		PORT_B,SPLIT
	NOP
	CALL	BEEP
	GOTO 	MAIN_LOOP

; SWITCHER MENU SELECT USING SWKP SERIAL SIGNALS --------------------------------------

SENDSERIAL_U
	BSF		STATUS,RP0			; BANK-1
	BCF		PIE1,TMR1IE			; DISABLE TIMER0 INTERRUPTS.
	NOP
	BCF		STATUS,RP0			; BANK-0
	CALL	UPKEY				; SEND SERIAL DATA TO SWITCHER
	NOP
	GOTO	RELOAD_UP

; RECORDER SELECT ---------------------------------------------------------------------

RECKEY_UP
	BSF		STATUS,RP0			; BANK-1
	BCF		PIE1,TMR1IE			; DISABLE TIMER0 INTERRUPTS.
	NOP
	BCF		STATUS,RP0			; BANK-0

	BCF		PORT_D,REC_MENU
	NOP
	BCF		PORT_D,RECORD
	NOP
	BCF		PORT_D,REC_STOP
	NOP
	BCF		PORT_D,REC_DOWN
	NOP
	BSF		PORT_D,REC_UP		; CLOSE UP RELAY
	NOP
	CALL	KEYDELAY
	BCF		PORT_D,REC_UP
	NOP
	GOTO	RELOAD_UP

; BRIGHTNESS SELECT -------------------------------------------------------------------

BRIGHT_UP
	BSF		STATUS,RP0			; BANK-1
	BCF		PIE1,TMR1IE			; DISABLE TIMER1 INTERRUPT
	BCF		STATUS,RP0			; BANK-0
	CALL	DIM_PLUS
	NOP
	GOTO	RELOAD_UP

; VOLUME SELECT -------------------------------------------------------------------

VOLUME_UP
	BSF		PORT_C,VOL_CLK
	NOP
	BSF		PORT_C,VOL_SW		; CONNECT VOL CLK TO UP/DN PIN
	NOP
	NOP
	BCF		PORT_C,VOL_SW		; VOL TO FLOAT
	NOP

	BSF		PORT_C,VOL_CLK
	NOP
	BSF		PORT_C,VOL_SW		; CONNECT VOL CLK TO UP/DN PIN
	NOP
	NOP
	BCF		PORT_C,VOL_SW		; VOL TO FLOAT
	NOP

; RELOAD INTERRUT TIMER ---------------------------------------------------------------

RELOAD_UP
	BTFSC	TIME_FLAGS,S_FLAG
	GOTO	TIME2_UP

	BTFSC	TIME_FLAGS,R_FLAG
	GOTO	TIME2_UP

	MOVLW	T1_VALUE			; LOAD INTERRUPT DELAY TIMER
	MOVWF	T1COUNT
	GOTO	RELOAD_UP2

TIME2_UP
	MOVLW	T2_VALUE			; LOAD INTERRUPT DELAY TIMER
	MOVWF	T1COUNT

RELOAD_UP2
	BCF		PIR1,TMR1IF			; CLEAR INT FLAG
	NOP
	CLRF	TMR1L
	CLRF	TMR1H
	NOP
	MOVLW	B'00110101'			; CLOCK = OSC/4, PRESCALER = 8
	MOVWF	T1CON				; TIMER1 SETUP

	BSF		STATUS,RP0			; BANK-1
	BSF		PIE1,TMR1IE			; ENABLE INTERRUPTS
	NOP							; TMR1 WILL INTERRUPT AGAIN.
	BCF		STATUS,RP0			; BANK-0
	BSF		T1CON,TMR1ON
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; LEFT ARROW KEY - CODE DD
;======================================================================================
KEY_DD
	BTFSS 	FLAGS,PWR_FLAG
	GOTO 	MAIN_LOOP

	BTFSS	FLAGS,CAM_FLAG
	GOTO	LEFT_CAM

	BTFSC	TIME_FLAGS,S_FLAG
	GOTO	SENDSERIAL_L

; CAMERA MODE -------------------------------------------------------------------------

LEFT_CAM
	MOVFW 	PORT_B
	MOVWF 	TEMP
	BTFSS 	TEMP,LEFT
	GOTO 	LEFT_OFF

	BCF 	PORT_B,LEFT 		; LEFT IS NOW OFF
	CALL	BEEP
	GOTO 	MAIN_LOOP

LEFT_OFF
	BSF 	PORT_B,LEFT	 		; LEFT IS NOW ON
	NOP
	BCF		PORT_B,DOWN
	NOP
	BCF		PORT_B,UP
	NOP
	BCF		PORT_B,RIGHT
	NOP
	BCF		PORT_B,SPLIT
	NOP
	CALL	BEEP
	GOTO 	MAIN_LOOP

; SWITCHER SELECT ---------------------------------------------------------------------

SENDSERIAL_L
	BSF		STATUS,RP0			; BANK-1
	BCF		PIE1,TMR1IE			; DISABLE TIMER0 INTERRUPT
	BCF		STATUS,RP0			; BANK-0
	CALL	LEFTKEY				; SEND SERIAL DATA TO SWITCHER
	
; RE-START TIMER-1. A KEY WAS PRESSED --------------------------------------------------

	MOVLW	T1_VALUE			; LOAD INTERRUPT DELAY TIMER
	MOVWF	T1COUNT
	BCF		PIR1,TMR1IF			; CLEAR INT FLAG
	NOP
	CLRF	TMR1L
	CLRF	TMR1H
	NOP
	MOVLW	B'00110101'			; CLOCK = OSC/4, PRESCALER = 8
	MOVWF	T1CON				; TIMER1 SETUP

	BSF		STATUS,RP0			; BANK-1
	BSF		PIE1,TMR1IE			; ENABLE INTERRUPTS
	NOP							; TMR0 WILL INTERRUPT AGAIN.
	BCF		STATUS,RP0			; BANK-0
	BSF		T1CON,TMR1ON
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; SPLIT KEY - CODE DB
;======================================================================================
KEY_DB
	BTFSS 	FLAGS,PWR_FLAG
	GOTO 	MAIN_LOOP

	BSF		FLAGS,CAM_FLAG		; DEFAULT SELECT CAMERAS.
	NOP
	MOVFW 	PORT_B
	MOVWF 	TEMP
	BTFSS 	TEMP,SPLIT
	GOTO 	SPLIT_OFF

	BCF 	PORT_B,SPLIT 		; SPLIT IS NOW OFF
	CALL	BEEP
	GOTO 	MAIN_LOOP

SPLIT_OFF
	BSF 	PORT_B,SPLIT 		; SPLIT IS NOW ON
	NOP
	BCF		PORT_B,DOWN
	NOP
	BCF		PORT_B,UP
	NOP
	BCF		PORT_B,RIGHT
	NOP
	BCF		PORT_B,LEFT
	NOP
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; RIGHT KEY - CODE D7
;======================================================================================
KEY_D7
	BTFSS 	FLAGS,PWR_FLAG
	GOTO 	MAIN_LOOP

	BTFSC	FLAGS,CAM_FLAG
	GOTO	RIGHT_CAM

	BTFSC	TIME_FLAGS,S_FLAG
	GOTO	SENDSERIAL_R

; CAMERA MODE -------------------------------------------------------------------------

RIGHT_CAM
	MOVFW 	PORT_B
	MOVWF 	TEMP
	BTFSS 	TEMP,RIGHT
	GOTO 	RIGHT_OFF

	BCF 	PORT_B,RIGHT 		; RIGHT IS NOW OFF
	CALL	BEEP
	GOTO 	MAIN_LOOP

RIGHT_OFF
	BSF 	PORT_B,RIGHT	 	; RIGHT IS NOW ON
	NOP
	BCF		PORT_B,DOWN
	NOP
	BCF		PORT_B,UP
	NOP
	BCF		PORT_B,LEFT
	NOP
	BCF		PORT_B,SPLIT
	NOP
	CALL	BEEP
	GOTO 	MAIN_LOOP

; SWITCHER SELECT ---------------------------------------------------------------------

SENDSERIAL_R
	BSF		STATUS,RP0			; BANK-1
	BCF		PIE1,TMR1IE			; DISABLE TIMER1 INTERRUPT
	BCF		STATUS,RP0			; BANK-0
	NOP
	CALL	RIGHTKEY			; SEND SERIAL DATA TO SWITCHER

; RE-START TIMER-1. A KEY WAS PRESSED --------------------------------------------------

	MOVLW	T1_VALUE			; LOAD INTERRUPT DELAY TIMER
	MOVWF	T1COUNT
	BCF		PIR1,TMR1IF			; CLEAR INT FLAG
	NOP
	CLRF	TMR1L
	CLRF	TMR1H
	NOP
	MOVLW	B'00110101'			; CLOCK = OSC/4, PRESCALER = 8
	MOVWF	T1CON				; TIMER1 SETUP

	BSF		STATUS,RP0			; BANK-1
	BSF		PIE1,TMR1IE			; ENABLE INTERRUPTS
	NOP							; TMR1 WILL INTERRUPT AGAIN.
	BCF		STATUS,RP0			; BANK-0
	BSF		T1CON,TMR1ON
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; HEAT KEY - CODE DF - TOGGLE ROUTINE
;======================================================================================
KEY_DF
	BSF		FLAGS,CAM_FLAG		; DEFAULT SELECT CAMERAS.
	NOP
	MOVFW 	PORT_A
	MOVWF 	TEMP
	BTFSS 	TEMP,HEAT
	GOTO 	HEAT_OFF
	BCF 	PORT_A,HEAT 		; HEAT IS NOW OFF
	CALL	BEEP
	GOTO 	MAIN_LOOP

HEAT_OFF
	BSF 	PORT_A,HEAT 		; HEAT IS NOW ON
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; DOWN ARROW KEY - CODE BE
;======================================================================================
KEY_BE
	BTFSS 	FLAGS,PWR_FLAG
	GOTO 	MAIN_LOOP
	
	BTFSC	FLAGS,CAM_FLAG
	GOTO	BACK_CAM

	BTFSC	TIME_FLAGS,S_FLAG
	GOTO	SENDSERIAL_D

	BTFSC	TIME_FLAGS,R_FLAG
	GOTO	RECKEY_DN

	BTFSC	TIME_FLAGS,B_FLAG
	GOTO	BRIGHT_DOWN

	BTFSC	TIME_FLAGS,V_FLAG
	GOTO	VOLUME_DOWN

; CAMERA MODE -------------------------------------------------------------------------

BACK_CAM
	MOVFW 	PORT_B
	MOVWF 	TEMP
	BTFSS 	TEMP,DOWN
	GOTO 	DOWN_OFF

	BCF 	PORT_B,DOWN 		; DOWN IS NOW OFF
	NOP
	CALL	BEEP
	GOTO 	MAIN_LOOP

DOWN_OFF
	BSF 	PORT_B,DOWN	 		; DOWN IS NOW ON
	NOP
	BCF		PORT_B,UP
	NOP
	BCF		PORT_B,LEFT
	NOP
	BCF		PORT_B,RIGHT
	NOP
	BCF		PORT_B,SPLIT
	NOP
	CALL	BEEP
	GOTO 	MAIN_LOOP

; SWITCHER MENU SELECT ----------------------------------------------------------------

SENDSERIAL_D
	BSF		STATUS,RP0			; BANK-1
	BCF		PIE1,TMR1IE			; DISABLE TIMER1 INTERRUPT
	NOP
	BCF		STATUS,RP0			; BANK-0
	CALL	DOWNKEY				; SEND SERIAL DATA TO SWITCHER
	NOP
	GOTO	RELOAD_DOWN

; RECORDER SELECT ---------------------------------------------------------------------

RECKEY_DN
	BSF		STATUS,RP0			; BANK-1
	BCF		PIE1,TMR1IE			; DISABLE TIMER1 INTERRUPT
	NOP
	BCF		STATUS,RP0			; BANK-0
	BCF		PORT_D,REC_MENU
	NOP
	BCF		PORT_D,RECORD
	NOP
	BCF		PORT_D,REC_STOP
	NOP
	BCF		PORT_D,REC_UP
	NOP
	BSF		PORT_D,REC_DOWN		; CLOSE DOWN RELAY
	NOP
	CALL	KEYDELAY
	BCF		PORT_D,REC_DOWN
	NOP
	GOTO	RELOAD_DOWN

; BRIGHTNESS SELECT -------------------------------------------------------------------

BRIGHT_DOWN
	BSF		STATUS,RP0			; BANK-1
	BCF		PIE1,TMR1IE			; DISABLE TIMER1 INTERRUPT
	BCF		STATUS,RP0			; BANK-0
	CALL	DIM_MINUS
	NOP
	GOTO	RELOAD_DOWN

; VOLUME SELECT -------------------------------------------------------------------

VOLUME_DOWN
	BCF		PORT_C,VOL_CLK
	NOP
	BSF		PORT_C,VOL_SW		; CONNECT VOL CLK TO UP/DN PIN
	NOP
	NOP
	BCF		PORT_C,VOL_SW		; VOL TO FLOAT
	NOP

	BCF		PORT_C,VOL_CLK
	NOP
	BSF		PORT_C,VOL_SW		; CONNECT VOL CLK TO UP/DN PIN
	NOP
	NOP
	BCF		PORT_C,VOL_SW		; VOL TO FLOAT
	NOP

; RE-START TIMER-1. A KEY WAS PRESSED --------------------------------------------------

RELOAD_DOWN
	BTFSC	TIME_FLAGS,S_FLAG
	GOTO	TIME2_DN

	BTFSC	TIME_FLAGS,R_FLAG
	GOTO	TIME2_DN

	MOVLW	T1_VALUE			; LOAD INTERRUPT DELAY TIMER
	MOVWF	T1COUNT
	GOTO	RELOAD_DN2

TIME2_DN
	MOVLW	T2_VALUE			; LOAD INTERRUPT DELAY TIMER
	MOVWF	T1COUNT

RELOAD_DN2
	BCF		PIR1,TMR1IF			; CLEAR INT FLAG
	NOP
	CLRF	TMR1L
	CLRF	TMR1H
	NOP
	MOVLW	B'00110101'			; CLOCK = OSC/4, PRESCALER = 8
	MOVWF	T1CON				; TIMER1 SETUP

	BSF		STATUS,RP0			; BANK-1
	BSF		PIE1,TMR1IE			; ENABLE INTERRUPTS
	NOP							; TMR1 WILL INTERRUPT AGAIN.
	BCF		STATUS,RP0			; BANK-0
	BSF		T1CON,TMR1ON
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; SWITCHER MENU KEY - CODE BD - THIS IS A MULTI FUNCTION KEY..!
;======================================================================================
KEY_BD
	BTFSS 	FLAGS,PWR_FLAG		; CHECK POWER IS ON.
	GOTO 	MAIN_LOOP			; NOPE...GO AWAY..!

	BSF		STATUS,RP0			; BANK-1
	BCF		PIE1,TMR1IE			; DISABLE TIMER0 INTERRUPT
	NOP
	BCF		STATUS,RP0			; BANK-0
	BCF		FLAGS,CAM_FLAG
	NOP
	CLRF	TIME_FLAGS			; CLEAR ALL FLAGS.
	BSF 	TIME_FLAGS,S_FLAG	; SET S_MENU FLAG
	NOP
	BCF		PORT_B,UP
	NOP
	BCF		PORT_B,DOWN
	NOP
	BCF		PORT_B,LEFT
	NOP
	BCF		PORT_B,RIGHT
	NOP
	BCF		PORT_B,SPLIT
	NOP	
	CALL	MENUKEY				; SEND SERIAL DATA TO SWITCHER
	
; RE-START TIMER-1. A KEY WAS PRESSED --------------------------------------------------

	MOVLW	T2_VALUE			; LOAD INTERRUPT DELAY TIMER
	MOVWF	T1COUNT
	BCF		PIR1,TMR1IF			; CLEAR INT FLAG
	NOP
	CLRF	TMR1L
	CLRF	TMR1H
	NOP
	MOVLW	B'00110101'			; CLOCK = OSC/4, PRESCALER = 8
	MOVWF	T1CON				; TIMER1 SETUP

	BSF		STATUS,RP0			; BANK-1
	BSF		PIE1,TMR1IE			; ENABLE INTERRUPTS
	NOP							; TMR1 WILL INTERRUPT AGAIN.
	BCF		STATUS,RP0			; BANK-0
	BSF		T1CON,TMR1ON
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; AV KEY - CODE BB
;======================================================================================
KEY_BB
	BTFSS 	FLAGS,PWR_FLAG
	GOTO 	MAIN_LOOP

	BSF		FLAGS,CAM_FLAG		; DEFAULT SELECT CAMERAS.
	NOP
	BCF		PORT_B,UP
	NOP
	BCF		PORT_B,DOWN
	NOP
	BCF		PORT_B,LEFT
	NOP
	BCF		PORT_B,RIGHT
	NOP
	BCF		PORT_B,SPLIT
	NOP	
	CALL	AVKEY
	NOP
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; QUAD KEY - CODE B7
;======================================================================================
KEY_B7
	BTFSS 	FLAGS,PWR_FLAG
	GOTO 	MAIN_LOOP

	BSF		FLAGS,CAM_FLAG		; DEFAULT SELECT CAMERAS.
	NOP	
	BCF		PORT_B,UP
	NOP
	BCF		PORT_B,DOWN
	NOP
	BCF		PORT_B,LEFT
	NOP
	BCF		PORT_B,RIGHT
	NOP
	BCF		PORT_B,SPLIT
	NOP
	CALL	QUADKEY
	NOP
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; SW SEL/JUMP KEY - CODE BF
;======================================================================================
KEY_BF
	BTFSS 	FLAGS,PWR_FLAG
	GOTO 	MAIN_LOOP

	BSF		FLAGS,CAM_FLAG		; DEFAULT SELECT CAMERAS.
	NOP
	BCF		PORT_B,UP
	NOP
	BCF		PORT_B,DOWN
	NOP
	BCF		PORT_B,LEFT
	NOP
	BCF		PORT_B,RIGHT
	NOP
	BCF		PORT_B,SPLIT
	NOP
	CALL	JUMPKEY
	NOP
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; REC MENU KEY - CODE 7E
;======================================================================================
KEY_7E
;	BTFSS	FLAGS,FLEET_FLAG	; FLEET FLAG SET..?
;	GOTO	MENU_ALLOWED
;	GOTO 	MAIN_LOOP			; FLEET FLAG IS SET SO IGNORE THIS KEY

;MENU_ALLOWED
	BTFSS 	FLAGS,PWR_FLAG
	GOTO 	MAIN_LOOP

	BSF		STATUS,RP0			; BANK-1
	BCF		PIE1,TMR1IE			; DISABLE TIMER1 INTERRUPT

	BCF		STATUS,RP0			; BANK-0
	BCF		FLAGS,CAM_FLAG
	NOP
	CLRF	TIME_FLAGS			; CLEAR ALL FLAGS.
	BSF 	TIME_FLAGS,R_FLAG	; SET R_MENU FLAG FOR UP/DN BUTTONS
	NOP
	BCF		PORT_D,REC_UP
	NOP
	BCF		PORT_D,RECORD
	NOP
	BCF		PORT_D,REC_STOP
	NOP
	BCF		PORT_D,REC_DOWN
	NOP

	BSF		PORT_D,REC_MENU		; CLOSE MENU SWITCH
	NOP
	CALL	KEYDELAY			; DELAY FOR 1 SEC.
	BCF		PORT_D,REC_MENU		; OPEN MENU SWITCH
	NOP

; RE-START TIMER-1. A KEY WAS PRESSED --------------------------------------------------

	MOVLW	T2_VALUE			; LOAD INTERRUPT DELAY TIMER
	MOVWF	T1COUNT
	BCF		PIR1,TMR1IF			; CLEAR INT FLAG
	NOP
	CLRF	TMR1L
	CLRF	TMR1H
	NOP
	MOVLW	B'00110101'			; CLOCK = OSC/4, PRESCALER = 8
	MOVWF	T1CON				; TIMER1 SETUP

	BSF		STATUS,RP0			; BANK-1
	BSF		PIE1,TMR1IE			; ENABLE INTERRUPTS
	NOP							; TMR1 WILL INTERRUPT AGAIN.
	BCF		STATUS,RP0			; BANK-0
	BSF		T1CON,TMR1ON
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; RECORD KEY - CODE 7D
;======================================================================================
KEY_7D
;	BTFSS	FLAGS,FLEET_FLAG	; FLEET FLAG SET..?
;	GOTO	REC_ALLOWED
;	GOTO 	MAIN_LOOP			; FLEET FLAG IS SET SO IGNORE THIS KEY

;REC_ALLOWED
	BTFSS 	FLAGS,PWR_FLAG
	GOTO 	MAIN_LOOP

	BSF		STATUS,RP0			; BANK-1
	BCF		PIE1,TMR1IE			; DISABLE TIMER1 INTERRUPT
	NOP
	BCF		STATUS,RP0			; BANK-0

	BCF		FLAGS,CAM_FLAG
	NOP
	BCF		PORT_D,REC_MENU
	NOP
	BCF		PORT_D,REC_UP
	NOP
	BCF		PORT_D,REC_STOP
	NOP
	BCF		PORT_D,REC_DOWN
	NOP
	BSF		PORT_D,RECORD		; CLOSE MENU SWITCH
	NOP
	CALL	KEYDELAY			; DELAY FOR 1 SEC.
	BCF		PORT_D,RECORD		; OPEN MENU SWITCH
	NOP

; RE-START TIMER-1. A KEY WAS PRESSED --------------------------------------------------

	MOVLW	T1_VALUE			; LOAD INTERRUPT DELAY TIMER
	MOVWF	T1COUNT
	BCF		PIR1,TMR1IF			; CLEAR INT FLAG
	NOP
	CLRF	TMR1L
	CLRF	TMR1H
	NOP
	MOVLW	B'00110101'			; CLOCK = OSC/4, PRESCALER = 8
	MOVWF	T1CON				; TIMER1 SETUP

	BSF		STATUS,RP0			; BANK-1
	BSF		PIE1,TMR1IE			; ENABLE INTERRUPTS
	NOP							; TMR1 WILL INTERRUPT AGAIN.
	BCF		STATUS,RP0			; BANK-0
	BSF		T1CON,TMR1ON
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; STOP KEY - CODE 7B
;======================================================================================
KEY_7B
;	BTFSS	FLAGS,FLEET_FLAG	; FLEET FLAG SET..?
;	GOTO	STOP_ALLOWED
;	GOTO 	MAIN_LOOP			; FLEET FLAG IS SET SO IGNORE THIS KEY

;STOP_ALLOWED
	BTFSS 	FLAGS,PWR_FLAG
	GOTO 	MAIN_LOOP

	BSF		STATUS,RP0			; BANK-1
	BCF		PIE1,TMR1IE			; DISABLE TIMER0 INTERRUPT
	BCF		STATUS,RP0			; BANK-0
	BSF		PORT_D,REC_STOP		; CLOSE MENU SWITCH
	NOP
	CALL	KEYDELAY			; DELAY FOR 1 SEC.
	BCF		PORT_D,REC_STOP		; OPEN MENU SWITCH
	NOP

; RE-START TIMER-1. A KEY WAS PRESSED --------------------------------------------------

	MOVLW	T1_VALUE				; LOAD INTERRUPT DELAY TIMER
	MOVWF	T1COUNT
	BCF		PIR1,TMR1IF			; CLEAR INT FLAG
	NOP
	CLRF	TMR1L
	CLRF	TMR1H
	NOP
	MOVLW	B'00110101'			; CLOCK = OSC/4, PRESCALER = 8
	MOVWF	T1CON				; TIMER1 SETUP

	BSF		STATUS,RP0			; BANK-1
	BSF		PIE1,TMR1IE			; ENABLE INTERRUPTS
	NOP							; TMR1 WILL INTERRUPT AGAIN.
	BCF		STATUS,RP0			; BANK-0
	BSF		T1CON,TMR1ON
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; AUDIO ON/OFF KEY - CODE 77 - TOGGLE ROUTINE
;======================================================================================
KEY_77
	BCF		STATUS,RP0
	BTFSS 	FLAGS,PWR_FLAG
	GOTO 	MAIN_LOOP

	BTFSC	FLAGS,AUDIO_FLAG	; AUDIO ON..?
	GOTO	AUDIO_WAS_ON

; AUDIO WAS OFF SO TURN AUDIO ON

	BSF		FLAGS,AUDIO_FLAG
	NOP
	BCF		PORT_B, BEEP_SW
	NOP
	BSF		PORT_B,AUDIO_SW
	NOP
	BCF		PORT_C,MODE_STBY
	NOP
	BSF		PORT_D,MODE_RUN
	NOP
	CALL	BEEP	
	GOTO 	MAIN_LOOP

; TURN AUDIO OFF

AUDIO_WAS_ON
	BCF		FLAGS,AUDIO_FLAG
	NOP
	BCF		PORT_B, BEEP_SW
	NOP
	BCF		PORT_B,AUDIO_SW
	NOP
	BSF		PORT_C,MODE_STBY
	NOP
	BCF		PORT_D,MODE_RUN
	NOP
	CALL	BEEP	
	GOTO 	MAIN_LOOP

;======================================================================================
; AUDIO VOLUME KEY - CODE 7F
;======================================================================================
KEY_7F
	BTFSS 	FLAGS,PWR_FLAG
	GOTO 	MAIN_LOOP

	BSF		STATUS,RP0			; BANK-1
	BCF		PIE1,TMR1IE			; DISABLE TIMER0 INTERRUPT
	BCF		STATUS,RP0			; BANK-0
	CLRF	TIME_FLAGS			; CLEAR ALL FLAGS.
	BSF 	TIME_FLAGS,V_FLAG	; SET V_FLAG FOR UP/DOWN BUTTONS
	NOP							; TEST_TIMER CLEARS THE V_FLAG OM TIMEOUT.
	BCF		FLAGS,CAM_FLAG
	NOP

; RE-START TIMER-1. A KEY WAS PRESSED --------------------------------------------------

	MOVLW	T1_VALUE			; LOAD INTERRUPT DELAY TIMER
	MOVWF	T1COUNT
	BCF		PIR1,TMR1IF			; CLEAR INT FLAG
	NOP
	CLRF	TMR1L
	CLRF	TMR1H
	NOP
	MOVLW	B'00110101'			; CLOCK = OSC/4, PRESCALER = 8
	MOVWF	T1CON				; TIMER1 SETUP

	BSF		STATUS,RP0			; BANK-1
	BSF		PIE1,TMR1IE			; ENABLE INTERRUPTS
	NOP							; TMR1 WILL INTERRUPT AGAIN.
	BCF		STATUS,RP0			; BANK-0
	BSF		T1CON,TMR1ON
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; BRIGHTNESS KEY - CODE DE (MULTIFUNCTION ROUTINE)
;======================================================================================
KEY_DE
	BTFSS 	FLAGS,PWR_FLAG
	GOTO 	MAIN_LOOP

	BSF		STATUS,RP0			; BANK-1
	BCF		PIE1,TMR1IE			; DISABLE TIMER1 INTERRUPT
	BCF		STATUS,RP0			; BANK-0
	CLRF	TIME_FLAGS			; CLEAR ALL FLAGS.
	BSF 	TIME_FLAGS,B_FLAG	; SET B_FLAG FOR UP/DOWN BUTTONS
	NOP							; TEST_TIMER CLEARS THE B_FLAG OM TIMEOUT.
	BCF		FLAGS,CAM_FLAG
	NOP

; RE-START TIMER-1. A KEY WAS PRESSED -------------------------------------------------

	MOVLW	T1_VALUE			; LOAD INTERRUPT DELAY TIMER
	MOVWF	T1COUNT
	BCF		PIR1,TMR1IF			; CLEAR INT FLAG
	NOP
	CLRF	TMR1L
	CLRF	TMR1H
	NOP
	MOVLW	B'00110101'			; CLOCK = OSC/4, PRESCALER = 8
	MOVWF	T1CON				; TIMER1 SETUP

	BSF		STATUS,RP0			; BANK-1
	BSF		PIE1,TMR1IE			; ENABLE INTERRUPTS
	NOP							; TMR1 WILL INTERRUPT AGAIN.
	BCF		STATUS,RP0			; BANK-0
	BSF		T1CON,TMR1ON
	CALL	BEEP
	GOTO 	MAIN_LOOP

;======================================================================================
; DIM(-) KEY - SUBROUTINE - TRIGGERED BY DOWN KEY
;======================================================================================
DIM_MINUS
	MOVFW 	DAC_DATA
	MOVWF 	TEMP
	MOVLW 	DEC_VALUE
	SUBWF 	TEMP,F
	BTFSS 	STATUS,C
	GOTO 	DIM_UNDER
	MOVFW 	TEMP
	MOVWF 	DAC_DATA
	CALL 	WR_DAC				; CALLING DAC_WR
	NOP
	RETLW	0

DIM_UNDER
	CLRF 	DAC_DATA
	CALL 	WR_DAC				; CALLING DAC_WR
	NOP
	RETLW	0

;======================================================================================
; DIM(+) KEY - SUBROUTINE - TRIGGERED BY UP KEY
;======================================================================================
DIM_PLUS
	MOVFW 	DAC_DATA
	MOVWF 	TEMP
	MOVLW 	INC_VALUE
	ADDWF 	TEMP,F
	BTFSC 	STATUS,C
	GOTO 	DIM_OVER
	MOVFW 	TEMP
	MOVWF 	DAC_DATA
	CALL 	WR_DAC				; CALLING DAC_WR
	NOP
	RETLW	0

DIM_OVER
	MOVLW 	0xFF
	MOVWF 	DAC_DATA			; CALLING DAC_WR
	CALL 	WR_DAC
	NOP
	RETLW	0

;======================================================================================
; DELAY560 - TIME DELAY SUBROUTINE - DELAY IS 561uSecs LONG.
;======================================================================================
DELAY560
	MOVLW 	0xB4				; 0xB9 = 561uSEC
	MOVWF 	DCOUNT
SERIAL1
	DECFSZ 	DCOUNT,F
	GOTO 	SERIAL1
	RETLW 	0

;======================================================================================
; DELAY200 - TIME DELAY SUBROUTINE 
;======================================================================================
DELAY200
	MOVLW 	0x41				; 0xB9 = 561uSEC
	MOVWF 	DCOUNT
SERIAL2
	DECFSZ 	DCOUNT,F
	GOTO 	SERIAL2
	RETLW 	0

;======================================================================================
; DELAY99 - TIME DELAY SUBROUTINE 
;======================================================================================
DELAY99
	MOVLW 	0x28				; 0xB9 = 561uSEC
	MOVWF 	DCOUNT
SERIAL3
	DECFSZ 	DCOUNT,F
	GOTO 	SERIAL3
	RETLW 	0

;======================================================================================
; STARTBIT - SUBROUTINE - SENDS A 9000uSec PULSE THEN A 45000uSec ZERO...
;======================================================================================
STARTBIT
	MOVLW	0x0F
	MOVWF	BITCOUNT
	BSF		PORT_E,SERIAL_OUT	; PORT PIN HIGH
	CALL	DELAY560			; PADDING FOR THE TIMING
	CALL	DELAY200

START_LOOP
	CALL	DELAY560
	DECFSZ	BITCOUNT,F
	GOTO	START_LOOP

	BCF		PORT_E,SERIAL_OUT	; PORT PIN LOW
	MOVLW	0x08
	MOVWF	BITCOUNT
	CALL	DELAY99

START_LOOP2
	CALL	DELAY560
	DECFSZ	BITCOUNT,F
	GOTO	START_LOOP2
	BSF		PORT_E,SERIAL_OUT	; PORT PIN LOW
	RETLW	0

;======================================================================================
; ONE - SUBROUTINE - SENDS A 560uSec PULSE THEN A 1690uSec ZERO...
;======================================================================================
ONE
	BSF		PORT_E, SERIAL_OUT	; PORT PIN HIGH
	CALL	DELAY560

	MOVLW	0x03
	MOVWF	BITCOUNT
	BCF		PORT_E,SERIAL_OUT	; PORT PIN LOW

ONE_LOOP
	CALL	DELAY560
	DECFSZ	BITCOUNT,F
	GOTO	ONE_LOOP
	RETLW	0

;======================================================================================
; ZERO- SUBROUTINE - SENDS A 560uSec PULSE THEN A 560uSec ZERO...
;======================================================================================
ZERO
	BSF		PORT_E,SERIAL_OUT	; PORT PIN HIGH
	CALL	DELAY560
	BCF		PORT_E,SERIAL_OUT	; PORT PIN LOW
	CALL	DELAY560
	RETLW	0

;======================================================================================
; STOPBIT - SUBROUTINE - SENDS A 560uSec PULSE ONLY
;======================================================================================
STOPBIT
	BSF		PORT_E,SERIAL_OUT	; PORT PIN HIGH
	CALL	DELAY560
	BCF		PORT_E,SERIAL_OUT	; PORT PIN LOW
	RETLW	0

;======================================================================================
; MENUKEY - SUBROUTINE
;======================================================================================
MENUKEY
	CALL	STARTBIT
	CALL 	ZERO				; BIT-1 	ADDRESS
	CALL 	ZERO				; BIT-2
	CALL 	ZERO				; BIT-3
	CALL 	ZERO				; BIT-4

	CALL 	ZERO				; BIT-5
	CALL 	ZERO				; BIT-6
	CALL 	ZERO				; BIT-7
	CALL 	ONE					; BIT-8

	CALL	ONE					; BIT-9		ADDRESS
	CALL 	ONE					; BIT-10
	CALL	ONE					; BIT-11
	CALL 	ONE					; BIT-12

	CALL	ONE					; BIT-13
	CALL	ONE					; BIT-14
	CALL 	ONE					; BIT-15
	CALL	ONE					; BIT-16

	CALL	ZERO				; BIT-17	DATA TRUE
	CALL	ONE					; BIT-18
	CALL	ONE					; BIT-19
	CALL	ONE					; BIT-20

	CALL	ZERO				; BIT-21
	CALL	ZERO				; BIT-22
	CALL	ZERO				; BIT-23
	CALL	ZERO				; BIT-24

	CALL	ONE					; BIT-25	DATA INVERTED
	CALL	ZERO				; BIT-26
	CALL	ZERO				; BIT-27
	CALL	ZERO				; BIT-28

	CALL	ONE					; BIT-29
	CALL	ONE					; BIT-30
	CALL	ONE					; BIT-31
	CALL	ONE					; BIT-32
	
	CALL	STOPBIT
	RETURN

;======================================================================================
; LEFTKEY - SUBBROUTINE 
;======================================================================================
LEFTKEY
	CALL	STARTBIT
	CALL 	ZERO				; BIT-1 	ADDRESS
	CALL 	ZERO				; BIT-2
	CALL 	ZERO				; BIT-3
	CALL 	ZERO				; BIT-4

	CALL 	ZERO				; BIT-5
	CALL 	ZERO				; BIT-6
	CALL 	ZERO				; BIT-7
	CALL 	ONE					; BIT-8

	CALL	ONE					; BIT-9		ADDRESS
	CALL 	ONE					; BIT-10
	CALL	ONE					; BIT-11
	CALL 	ONE					; BIT-12

	CALL	ONE					; BIT-13
	CALL	ONE					; BIT-14
	CALL 	ONE					; BIT-15
	CALL	ONE					; BIT-16

	CALL	ZERO				; BIT-17	DATA TRUE
	CALL	ZERO				; BIT-18
	CALL	ONE					; BIT-19
	CALL	ZERO				; BIT-20

	CALL	ONE					; BIT-21
	CALL	ZERO				; BIT-22
	CALL	ZERO				; BIT-23
	CALL	ZERO				; BIT-24

	CALL	ONE					; BIT-25	DATA INVERTED
	CALL	ONE					; BIT-26
	CALL	ZERO				; BIT-27
	CALL	ONE					; BIT-28

	CALL	ZERO				; BIT-29
	CALL	ONE					; BIT-30
	CALL	ONE					; BIT-31
	CALL	ONE					; BIT-32
	
	CALL	STOPBIT
	RETURN

;======================================================================================
; RIGHTKEY - SUBROUTINE 
;======================================================================================
RIGHTKEY
	CALL	STARTBIT
	CALL 	ZERO				; BIT-1 	ADDRESS
	CALL 	ZERO				; BIT-2
	CALL 	ZERO				; BIT-3
	CALL 	ZERO				; BIT-4

	CALL 	ZERO				; BIT-5
	CALL 	ZERO				; BIT-6
	CALL 	ZERO				; BIT-7
	CALL 	ONE					; BIT-8

	CALL	ONE					; BIT-9		ADDRESS
	CALL 	ONE					; BIT-10
	CALL	ONE					; BIT-11
	CALL 	ONE					; BIT-12

	CALL	ONE					; BIT-13
	CALL	ONE					; BIT-14
	CALL 	ONE					; BIT-15
	CALL	ONE					; BIT-16

	CALL	ZERO				; BIT-17	DATA TRUE
	CALL	ZERO				; BIT-18
	CALL	ZERO				; BIT-19
	CALL	ZERO				; BIT-20

	CALL	ONE					; BIT-21
	CALL	ZERO				; BIT-22
	CALL	ZERO				; BIT-23
	CALL	ZERO				; BIT-24

	CALL	ONE					; BIT-25	DATA INVERTED
	CALL	ONE					; BIT-26
	CALL	ONE					; BIT-27
	CALL	ONE					; BIT-28

	CALL	ZERO				; BIT-29
	CALL	ONE					; BIT-30
	CALL	ONE					; BIT-31
	CALL	ONE					; BIT-32
	
	CALL	STOPBIT
	RETURN

;======================================================================================
; DOWNKEY - SUBROUTINE 
;======================================================================================
DOWNKEY
	CALL	STARTBIT
	CALL 	ZERO				; BIT-1 	ADDRESS
	CALL 	ZERO				; BIT-2
	CALL 	ZERO				; BIT-3
	CALL 	ZERO				; BIT-4

	CALL 	ZERO				; BIT-5
	CALL 	ZERO				; BIT-6
	CALL 	ZERO				; BIT-7
	CALL 	ONE					; BIT-8

	CALL	ONE					; BIT-9		ADDRESS
	CALL 	ONE					; BIT-10
	CALL	ONE					; BIT-11
	CALL 	ONE					; BIT-12

	CALL	ONE					; BIT-13
	CALL	ONE					; BIT-14
	CALL 	ONE					; BIT-15
	CALL	ONE					; BIT-16

	CALL	ONE					; BIT-17	DATA TRUE
	CALL	ONE					; BIT-18
	CALL	ONE					; BIT-19
	CALL	ZERO				; BIT-20

	CALL	ONE					; BIT-21
	CALL	ZERO				; BIT-22
	CALL	ZERO				; BIT-23
	CALL	ZERO				; BIT-24

	CALL	ZERO				; BIT-25	DATA INVERTED
	CALL	ZERO				; BIT-26
	CALL	ZERO				; BIT-27
	CALL	ONE					; BIT-28

	CALL	ZERO				; BIT-29
	CALL	ONE					; BIT-30
	CALL	ONE					; BIT-31
	CALL	ONE					; BIT-32
	
	CALL	STOPBIT
	RETURN

;======================================================================================
; UPKEY - SUBROUTINE 
;======================================================================================
UPKEY
	CALL	STARTBIT
	CALL 	ZERO				; BIT-1 	ADDRESS
	CALL 	ZERO				; BIT-2
	CALL 	ZERO				; BIT-3
	CALL 	ZERO				; BIT-4

	CALL 	ZERO				; BIT-5
	CALL 	ZERO				; BIT-6
	CALL 	ZERO				; BIT-7
	CALL 	ONE					; BIT-8

	CALL	ONE					; BIT-9		ADDRESS
	CALL 	ONE					; BIT-10
	CALL	ONE					; BIT-11
	CALL 	ONE					; BIT-12

	CALL	ONE					; BIT-13
	CALL	ONE					; BIT-14
	CALL 	ONE					; BIT-15
	CALL	ONE					; BIT-16

	CALL	ONE					; BIT-17	DATA TRUE
	CALL	ONE					; BIT-18
	CALL	ZERO				; BIT-19
	CALL	ZERO				; BIT-20

	CALL	ONE					; BIT-21
	CALL	ZERO				; BIT-22
	CALL	ZERO				; BIT-23
	CALL	ZERO				; BIT-24

	CALL	ZERO				; BIT-25	DATA INVERTED
	CALL	ZERO				; BIT-26
	CALL	ONE					; BIT-27
	CALL	ONE					; BIT-28

	CALL	ZERO				; BIT-29
	CALL	ONE					; BIT-30
	CALL	ONE					; BIT-31
	CALL	ONE					; BIT-32
	
	CALL	STOPBIT	
	RETURN

;======================================================================================
; AVKEY - SUBROUTINE 
;======================================================================================
AVKEY
	CALL	STARTBIT
	CALL 	ZERO				; BIT-1 	ADDRESS
	CALL 	ZERO				; BIT-2
	CALL 	ZERO				; BIT-3
	CALL 	ZERO				; BIT-4

	CALL 	ZERO				; BIT-5
	CALL 	ZERO				; BIT-6
	CALL 	ZERO				; BIT-7
	CALL 	ONE					; BIT-8

	CALL	ONE					; BIT-9		ADDRESS
	CALL 	ONE					; BIT-10
	CALL	ONE					; BIT-11
	CALL 	ONE					; BIT-12

	CALL	ONE					; BIT-13
	CALL	ONE					; BIT-14
	CALL 	ONE					; BIT-15
	CALL	ONE					; BIT-16

	CALL	ONE					; BIT-17	DATA TRUE
	CALL	ONE					; BIT-18
	CALL	ZERO				; BIT-19
	CALL	ONE					; BIT-20

	CALL	ZERO				; BIT-21
	CALL	ZERO				; BIT-22
	CALL	ZERO				; BIT-23
	CALL	ZERO				; BIT-24

	CALL	ZERO				; BIT-25	DATA INVERTED
	CALL	ZERO				; BIT-26
	CALL	ONE					; BIT-27
	CALL	ZERO				; BIT-28

	CALL	ONE					; BIT-29
	CALL	ONE					; BIT-30
	CALL	ONE					; BIT-31
	CALL	ONE					; BIT-32
	
	CALL	STOPBIT
	RETURN

;======================================================================================
; QUADKEY - SUBBROUTINE 
;======================================================================================
QUADKEY
	CALL	STARTBIT
	CALL 	ZERO				; BIT-1 	ADDRESS
	CALL 	ZERO				; BIT-2
	CALL 	ZERO				; BIT-3
	CALL 	ZERO				; BIT-4

	CALL 	ZERO				; BIT-5
	CALL 	ZERO				; BIT-6
	CALL 	ZERO				; BIT-7
	CALL 	ONE					; BIT-8

	CALL	ONE					; BIT-9		ADDRESS
	CALL 	ONE					; BIT-10
	CALL	ONE					; BIT-11
	CALL 	ONE					; BIT-12

	CALL	ONE					; BIT-13
	CALL	ONE					; BIT-14
	CALL 	ONE					; BIT-15
	CALL	ONE					; BIT-16

	CALL	ONE					; BIT-17	DATA TRUE
	CALL	ZERO				; BIT-18
	CALL	ZERO				; BIT-19
	CALL	ONE					; BIT-20

	CALL	ONE					; BIT-21
	CALL	ZERO				; BIT-22
	CALL	ZERO				; BIT-23
	CALL	ZERO				; BIT-24

	CALL	ZERO				; BIT-25	DATA INVERTED
	CALL	ONE					; BIT-26
	CALL	ONE					; BIT-27
	CALL	ZERO				; BIT-28

	CALL	ZERO				; BIT-29
	CALL	ONE					; BIT-30
	CALL	ONE					; BIT-31
	CALL	ONE					; BIT-32
	
	CALL	STOPBIT	
	RETURN

;======================================================================================
; JUMPKEY - SUBROUTINE 
;======================================================================================
JUMPKEY
	CALL	STARTBIT
	CALL 	ZERO				; BIT-1 	ADDRESS
	CALL 	ZERO				; BIT-2
	CALL 	ZERO				; BIT-3
	CALL 	ZERO				; BIT-4

	CALL 	ZERO				; BIT-5
	CALL 	ZERO				; BIT-6
	CALL 	ZERO				; BIT-7
	CALL 	ONE					; BIT-8

	CALL	ONE					; BIT-9		ADDRESS
	CALL 	ONE					; BIT-10
	CALL	ONE					; BIT-11
	CALL 	ONE					; BIT-12

	CALL	ONE					; BIT-13
	CALL	ONE					; BIT-14
	CALL 	ONE					; BIT-15
	CALL	ONE					; BIT-16

	CALL	ONE					; BIT-17	DATA TRUE
	CALL	ONE					; BIT-18
	CALL	ZERO				; BIT-19
	CALL	ONE					; BIT-20

	CALL	ONE					; BIT-21
	CALL	ZERO				; BIT-22
	CALL	ZERO				; BIT-23
	CALL	ZERO				; BIT-24

	CALL	ZERO				; BIT-25	DATA INVERTED
	CALL	ZERO				; BIT-26
	CALL	ONE					; BIT-27
	CALL	ZERO				; BIT-28

	CALL	ZERO				; BIT-29
	CALL	ONE					; BIT-30
	CALL	ONE					; BIT-31
	CALL	ONE					; BIT-32
	
	CALL	STOPBIT	
	RETURN

;======================================================================================
	END

;------------------------------------------------------------------------------------
;   KEYPAD ARRAY ADDRESSES:
;------------------------------------------------------------------------------------
; 
;--------------------------------------------
; 		KEY-1 	KEY-2 	KEY-3 	KEY-4	KEY-5
; 		EEH 	EDH 	EBH 	E7H		EFH
;
; 		KEY-6 	KEY-7 	KEY-8	KEY-9	KEY-10
; 		DEH 	DDH 	DBH 	D7H		DFH	
;
;		KEY-11 	KEY-12 	KEY-13 	KEY-14	KEY-15
; 		BEH 	BDH 	BBH 	B7H		BFH
;
; 		KEY-16 	KEY-17 	KEY-18 	KEY-19	KEY-20
; 		7EH 	7DH 	7BH 	77H		7FH
;
;------------------------------------------------------------------------------------
;	KEY		FUNCTION			CODE								
;------------------------------------------------------------------------------------
;
;	B1	=	POWER ON/OFF		EE		
;	B2  =	KILL				ED	
;	B3  =	MIRROR OM/OFF		EB	
;	B4  =	SHUTTERS			E7		
;	B5  =	UP ARROW			EF	
;	B6  =	BRITNESS			DE	
;	B7  =	LEFT ARROW			DD	
;	B8  =	SPLIT				DB	
;	B9  =	RIGHT ARROW			D7
;	B10 =	HEATERS				DF	
;	B11 =	DOWN ARROW			BE	
;	B12 =	SWITCHER MENU		BD	
;	B13 =	AV					BB	
;	B14 =	QUAD				B7	
;	B15 =	SELECT/JUMP			BF	
;	B16 =	RECORDER MENU		7E	
;	B17 =	RECORD/PAUSE		7D	
;	B18 =	STOP/BACK			7B	
;	B19 =	AUDIO ON/OFFF		77	
;	B20 =	AUDIO VOLUME		7F		
;
;----------------------------------------------------------------------------------