/*
 * File:   main.h
 * Author: filip
 *
 * Created on March 2, 2017, 3:02 PM
 */

#ifndef MAIN_H
#define	MAIN_H

#ifdef	__cplusplus
extern "C" {
#endif


#define _XTAL_FREQ 4000000
    //Constants

#define N_VALUE 	 0xAA 			// DEFAULT VALUE FOR NIGHT BRIGHTNESS
#define D_VALUE 	 0xFF           // DEFAULT VALUE FOR DAY BRIGHTNESS
#define INC_VALUE 	 0x10 			// DEFAULT FOR INCREMENT
#define DEC_VALUE 	 0x10 			// DEFAULT FOR DECREMENT
#define T1_VALUE	 0x15			// DELAY FOR BRIGHTNESS AND VOLUME CONTROLS
#define T2_VALUE	 0x2F			// DELAY FOR SWITCHER AND RECORDER MENUS
#define KEYDELAY    500             //Delay for rec menu key presses

//Ports

#define SHUT		RA0				// PORT-A BIT-0 - OUTPUT - 2N3904
#define PWR 		RA1 				// PORT-A BIT-1 - OUTPUT - 2N3904
#define MIRROR		RA2				// PORT_A BIT-2 - OUTPUT - 2N3904
#define HEAT 		RA3 				// PORT-A BIT-3 - OUTPUT - 2N3904
#define FLEET 		RA4 				// PORT-A BIT-4 - INPUT
#define KILL 		RA5 				// PORT-A BIT-5 - OUTPUT - DIGITAL 0=OFF 1=ON

#define UP			RB0				// PORT-B BIT-0 - OUTPUT - IN-1(U9) - MAX313
#define DOWN		RB1				// PORT-B,BIT-1 - OUTPUT - IN-2(U9) - MAX313
#define LEFT		RB2				// PORT-B BIT-2 - OUTPUT - IN-3(U9) - MAX313
#define RIGHT		RB3				// PORT-B BIT-3 - OUTPUT - IN-4(U9) - MAX313
#define SPLIT 		RB4 				// PORT-B BIT-4 - OUTPUT - IN-4(U6) - MAX313
#define BEEP_SW		RB5				// PORT-B BIT-5 - OUTPUT - IN-1(U3) - MAX313
#define AUDIO_SW	RB6				// PORT-B BIT-6 - OUTPUT - IN-2(U3) - MAX313
#define INT_DELAY	RB7				// PORT-B BIT-7 - OUTPUT - TO A LED. SHOWS INTERRUT TIME.

#define DAC_CS 		RC0 				// PORT-C BIT-0 - OUTPUT - DIGITAL 0=ON 1=OFF - MCP4801
#define DAC_SCK 	RC1 				// PORT-C BIT-1 - OUTPUT - DIGITAL I/O - MCP4801
#define DAC_SDI 	RC2 				// PORT-C BIT-2 - OUTPUT - DIGITAL I/O - MCP4801
#define DAC_LOAD	RC3				// PORT-C BIT-3 - OUTPUT - DIGITAL I/O - MCP4801
#define OP_CS		RC4				// PORT-C BIT-4 - OUTPUT - DIGITAL 0=ON 1=OFF - MCP603
#define VOL_CLK		RC5  				// PORT-C BIT-5 - OUTPUT - NO-1(U2) - MAX313
#define MODE_STBY	RC6				// PORT-C BIT-6 - OUTPUT - IN-2(U2) - MAX313
#define VOL_SW		RC7				// PORT-C BIT-7 - OUTPUT - IN-1(U2) - MAX313

#define REC_MENU	RD0				// PORT-D BIT-0 - OUTPUT - IN-3(U3) - MAX313
#define RECORD		RD1				// PORT-D BIT-1 - OUTPUT - IN-4(U3) - MAX313
#define REC_STOP	RD2				// PORT-D BIT-2 - OUTPUT - IN-1(U6) - MAX313
#define REC_UP		RD3				// PORT-D BIT-3 - OUTPUT - IN-2(U6) - MAX313
#define REC_DOWN	RD4				// PORT-D BIT-4 - OUTPUT - IN-3(U6) - MAX313
#define MODE_MUTE	RD5				// PORT-D BIT-5 - OUTPUT - IN-3(U2) - MAX313
#define MODE_RUN	RD6				// PORT-D BIT-6 - OUTPUT - IN-4(U2) - MAX313
#define BEEP_SIG	RD7				// PORT-D BIT-7 - OUTPUT - NO-1(U3) - MAX313

#define RX 			RE0 				// PORT-E BIT-0 - INPUT  - DIGITAL I/O - (SIN)
#define DX 			RE1 				// PORT-E BIT-1 - OUTPUT - DIGITAL I/O - (SOUT)
#define SERIAL_OUT	RE2				// PORT-E BIT-2 - OUTPUT - DIGITAL I/O - SERIAL OUT TO SWITCHER KEYPAD INPUT


//Directions
#define _UP 0
#define _DOWN 1
#define _LEFT 2
#define _RIGHT 3
#define _FRONT 0
#define _BACK 1
#define _SPLIT 5


#define _NONE 0xFF
//Serial Messages
#define _MENUKEY 4
#define _AVKEY 5
#define _QUADKEY 6
#define _JUMPKEY 7

void interrupt timerisr (void);
void init_system (void);
void WR_DAC (unsigned char DAC_DATA);
void DAC_bitbang (unsigned char, unsigned char);
void RCVR (void);
void XMTR (unsigned char XMTREG);
void ID_KEY (unsigned char data);
void togglepower (void);
void toggleaudio (void);
void cleartime (void);
void arrow (unsigned char direction);
void cam (unsigned char cam);
void reckey (unsigned char key);
void brightness(unsigned char direction);
void volume (unsigned char direction);
void beep (void);
void serialsend (unsigned long int data);
void ReloadTimer (unsigned char length);
void StartHeaterTimer (void);
#ifdef	__cplusplus
}
#endif

#endif	/* MAIN_H */
