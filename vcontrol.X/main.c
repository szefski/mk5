/*
 * File:   main.c
 * Author: filip
 *
 * Created on March 2, 2017, 3:01 PM
 */

// CONFIG
#pragma config FOSC = XT        // Oscillator Selection bits (XT oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config CP = OFF         // FLASH Program Memory Code Protection bit (Code protection off)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)


#include <xc.h>
#include "main.h"

/*
 *
 */
const unsigned long int Message[]={
0b11101100000100111111111110000000, //UP
0b11101000000101111111111110000000, //DOWN
0b11101011000101001111111110000000, //LEFT
0b11101111000100001111111110000000, //RIGHT
0b11110001000011101111111110000000, //MENUKEY
0b11110100000010111111111110000000, //AVKEY
0b11100110000110011111111110000000, //QUADKEY
0b11100100000110111111111110000000, //JUMPKEY
};

unsigned char T1COUNT = T1_VALUE;
unsigned char DAC_DATA;
unsigned char rec_menu_level = 0;
unsigned char AV_init = 0;
int HeaterCount;
//cycle clock is 1MHZ
//Timer 0 rolls over every 65536 cycles
//=65.536 ms per rollover
//915 rollovers per minute
#define HEATONCOUNT 4575   //3 minutes worth of rollovers
#define HEATOFFCOUNT 9150

struct {
unsigned RXF :1,DNF :1,PWRF :1,FLEETF :1,CAMF :1,AUDIOF :1,BEEPF :1,HEATF :1; // flag bits

} flags = {0,0,0,0,0,0,0,0};

struct {
unsigned S :1,R :1,V :1,B :1,A :1; // flag bits

} time = {0,0,0,0,0};

void interrupt timerisr (void)
{
    //Check where the interrupt is from
    if(PIR1bits.TMR1IF == 1)
    {
        T1COUNT--;

        PIE1bits.TMR1IE = 0;
        PIR1bits.TMR1IF = 0;
        T1CONbits.TMR1ON = 0;

        if(T1COUNT == 0)
        {
            INT_DELAY = 0;
            //CLRF TIME_FLAGS
            time.S = 0;
            time.R = 0;
            time.V = 0;
            time.B = 0;
            NOP();
            flags.CAMF = 1;
            NOP();
        }
        else
        {
            INT_DELAY = 1;
            TMR1L=0;
            NOP();
            TMR1H=0;
            NOP();
            T1CON = 0b00110101;
            PIE1bits.TMR1IE = 1;
            NOP();
            T1CONbits.TMR1ON = 1;
        }
    }
    else if(INTCONbits.TMR0IF == 1)  //Heater timer has triggered
    {
        INTCONbits.TMR0IE = 0;
        INTCONbits.TMR0IF = 0;
        if(flags.HEATF == 1)
        {
            //exit if the flag isn't set
            if(HEAT == 1)   //if the output is on, do the following checks
            {
                if(HeaterCount>HEATONCOUNT)
                {
                    HEAT = 0;
                    HeaterCount=0;
                }
                else
                    HeaterCount++;
            }
            else if(HEAT == 0)
            {
                if(HeaterCount>HEATOFFCOUNT)
                {
                    HEAT = 1;
                    HeaterCount = 1;
                }
                else
                    HeaterCount++;
            }
            
            StartHeaterTimer();
        }
        
        
    }
}


void main (void)
{
    init_system();
    /*
    if(FLEET == 0)
      flags.FLEETF = 1;
    else
      flags.FLEETF = 0;
    */
       flags.FLEETF = 0;    //Fleet is always off in this mode     
    //Main Loop
    while(1)
    {
        RCVR();
    }

}

void init_system (void)
{
    unsigned char x;

    STATUSbits.RP0 = 0;
    STATUSbits.RP1 = 0;
    PORTA = 0;
    PORTB = 0;
    PORTC = 0;
    PORTD = 0;
    PORTE = 0;

    STATUSbits.RP0 = 1;
    ADCON1 = 0x07;
    TRISA = 0x10;
    TRISB = 0x00;
    TRISC = 0x00;
    TRISD = 0x00;
    TRISE = 0x01;

    INTCONbits.TMR0IE = 0;
    INTCONbits.TMR0IF = 0;
    STATUSbits.RP0 = 0;
    T1CON = 0b00110101;
    INTCONbits.GIE = 1;
    NOP();
    INTCONbits.PEIE = 1;
    NOP();

    PORTA = 0;
    PORTB = 0;
    PORTC = 0;
    PORTD = 0;
    PORTE = 0;
    TMR1L = 0;
    TMR1H = 0;
    PIR1 = 0;


    DX = 1;
    NOP();
    HEAT = 0;
    PWR = 0;
    SERIAL_OUT = 0; //Base of transistor, 0 output is 5v output
    DAC_LOAD = 1;
    DAC_CS = 1;
    OP_CS = 1;
    flags.PWRF = 0;
    flags.CAMF = 1;
    flags.FLEETF = 0;


    for(x=15;x>0;x--)
    {
        volume(_UP);
        __delay_ms(50);
    }
    //Uncomment these lines if you value your hearing
    /*  
    for (x=5;x>0;x--)
    {
        volume(_DOWN);
    }
     */
    MODE_STBY = 0;
    NOP();

    // INITS THE DAC FOR DEFAULT ON DAY BRIGHTNESS -----------------------------------------
    DAC_DATA = D_VALUE;
    WR_DAC(DAC_DATA);
    flags.DNF = 1;
    //End of Init_System
}

void WR_DAC (unsigned char DAC_DATA)
{
    DAC_CS = 0;
    DAC_bitbang(0b00110000,4);  //Control nibble
    DAC_bitbang(DAC_DATA,8);    //Data Byte
    DAC_bitbang(0x00,4);        //Null nibble

    DAC_CS = 1;
    DAC_LOAD = 0;
    NOP();
    NOP();
    DAC_LOAD = 1;


}

void DAC_bitbang (unsigned char data,unsigned char bits)
{
    unsigned char x;
    DAC_SCK = 0;

    __delay_us(120);

    for(x=0;x<bits;x++)
    {
        if(data & (0x80>>x))
            DAC_SDI = 1;
        else
            DAC_SDI = 0;

        __delay_us(120);
        DAC_SCK = 1;
        __delay_us(120);
        DAC_SCK = 0;
    }

}

void RCVR (void)
{
    unsigned char RCVREG = 0;
    unsigned char x, temp;


    if(RX == 0)
    {
        RCVREG = 0;
        flags.RXF = 0;
        //Start bit detected
        //DX = 0;
        __delay_us(240);    //In Don's code, this is delay50
        //DX = !DX;
        __delay_us(120); //99 but actually 113
        for(x=0;x<8;x++)
        {
            RCVREG = RCVREG>>1;

            if(RX == 1)
            {
                RCVREG = RCVREG | 0b10000000;
            }

            //DX = !DX;
            __delay_us(240);

        }

        if(RX == 1)  //bad stop bit
        {
            flags.RXF = 0;
            //DX = !DX;

        }
        else
        {
            __delay_us(130);

            if(RX == 0)
                flags.RXF = 0; //bad stop
            else
            {
                //good stop
                DX = 0;
                __delay_us(217);
                DX = 1;
                flags.RXF = 1;
            }
        }

    }

    else
    {

    }


    if(flags.RXF == 1)
    {
       /* temp = RCVREG;
        temp = temp<<4;
        temp = temp & 0xF0;
        RCVREG = RCVREG>>4;
        RCVREG = RCVREG & 0x0F;
        RCVREG = RCVREG | temp; */
        //__delay_ms(100);
        //XMTR(RCVREG);
        ID_KEY(RCVREG);
        flags.RXF = 0;
    }

}


void XMTR (unsigned char XMTREG)
{
    //DX is 1
    //RX is 0
    unsigned char x;


    //Send Start Bit
    DX = 0;
    __delay_us(217);

    for(x=0;x<8;x++)
    {
        if((XMTREG & (0x01 << x)) == 0)
            DX = 0;
        else
            DX = 1;
        __delay_us(217);
    }

    DX = 0;
    __delay_us(217);
    DX = 1;



}

void ID_KEY (unsigned char data)
{
    switch(data)    //Do these checks even if power is off
    {
      case 0xEE:
          beep();
          togglepower();
          
          break;
      case 0xDF:
          beep();
          flags.CAMF = 1;         
          flags.HEATF = !flags.HEATF;
          if(flags.HEATF == 1)
          {
              HEAT = 1;
              StartHeaterTimer();
          }
          else  //it's 0
          {
              INTCONbits.TMR0IE = 0;
              INTCONbits.TMR0IF = 0;
              HEAT = 0;
          }
          cleartime();
          
          break;
    }

    if(flags.PWRF == 1) //Only do the following if PWRF = 1
    {
      switch(data)
      {

          case 0xED:  //MIRROR
              beep();
              flags.CAMF = 1;
              MIRROR = !MIRROR;
              cleartime();
              
              break;
          case 0xEB:  //KILL
              beep();
              flags.CAMF = 1;
              KILL = !KILL;
              cleartime();
              
              break;
          case 0xE7:  //SHUT
              beep();
              flags.CAMF = 1;
              SHUT = !SHUT;
              cleartime();
              
              break;
          case 0xEF:
              beep();
              arrow(_UP);

              if((time.S == 1) || (time.R == 1))
                  ReloadTimer(T2_VALUE);
              else if((time.B == 1) || (time.V == 1))
                  ReloadTimer(T1_VALUE);
              
              

              break;
  /*-----------------------------------------*/
          case 0xDE:
              //BRIGHTNESS MULTIFUNCTION
              beep();
              if(time.B == 0)
              {
                cleartime();
                time.B = 1;
                flags.CAMF = 0;
                ReloadTimer(T1_VALUE);
              }
              else
              {
                  cleartime();
                  flags.CAMF = 1;
              }
              
              
              break;
          case 0xDD:
              beep();
              arrow(_LEFT);
              //ReloadTimer(T1_VALUE);
              break;
          case 0xDB:
              //SPLIT KEY
              beep();
              flags.CAMF = 1;
              cam(_SPLIT);
              cleartime();
              break;
          case 0xD7:
              beep();
              arrow(_RIGHT);
              //ReloadTimer(T1_VALUE);
              
              break;

  /*-----------------------------------------*/
          case 0xBE:
              beep();
              arrow(_DOWN);

              if((time.S == 1) || (time.R == 1))
                  ReloadTimer(T2_VALUE);
              else if((time.B == 1) || (time.V == 1))
                  ReloadTimer(T1_VALUE);
              
              
              break;
          case 0xBD:
              //Switcher Key
              beep();
              PIE1bits.TMR1IE = 0;
              //if(time.S == 0)     //The switcher isn't reliable enough to work with absolutes like this.
              //{                   //It goes into a different menu, depending on the last button press.
                flags.CAMF = 0;
                cleartime();
                time.S = 1;
                cam(_NONE);
                serialsend(Message[_MENUKEY]);
                ReloadTimer(T2_VALUE);
             // }
              /*else
              {
                  cleartime();
                  flags.CAMF = 1;
                  serialsend(Message[_MENUKEY]);
              }
               */
              
              break;
              
          case 0xBB:
              beep();
              //AV KEY
              flags.CAMF = 1;
              cam(_NONE);
              //time.A maybe? It's too 
              if(time.R == 1)
                  //ReloadTimer(T1_VALUE);    //If we're in R mode, this button should restart the timer                
                  INT_DELAY = 1;
              else
                  cleartime();
              
              serialsend(Message[_AVKEY]);

              break;
          case 0xB7:
              //QUAD KEY
              beep();
              flags.CAMF = 1;
              cam(_NONE);
              cleartime();
              serialsend(Message[_QUADKEY]);
              break;
          case 0xBF:
              //SW SEL/JUMP KEY
              beep();
              flags.CAMF = 1;
              cam(_NONE);
              cleartime();
              serialsend(Message[_JUMPKEY]);
              break;
  /*-----------------------------------------*/
          case 0x7E:
              //REC MENU KEY
              PIE1bits.TMR1IE = 0;
              if(time.R == 0)
              { //We are now entering the rec menu
                beep();
                flags.CAMF = 0;
                cleartime();
                INT_DELAY = 1;
                time.R=1;   //No Timer in the rec menu
                
                REC_UP = 0;
                RECORD = 0;
                REC_STOP = 0;
                REC_DOWN = 0;
                REC_MENU = 1;
                __delay_ms(KEYDELAY);
                REC_MENU = 0;
                rec_menu_level = 1;
                //ReloadTimer(T2_VALUE);
              }
              
              else  //We are already in the rec menu!
              {
                beep();
                flags.CAMF = 1;
                cleartime();
                

              }
              
              break;
          case 0x7D:
              //RECORD KEY
              PIE1bits.TMR1IE = 0;
              flags.CAMF = 0;

              REC_UP = 0;
              REC_STOP = 0;
              REC_DOWN = 0;
              REC_MENU = 0;
              beep();
              RECORD = 1;
              __delay_ms(KEYDELAY);
              RECORD = 0;
              /*if(time.R == 1)
                ReloadTimer(T1_VALUE);*/
              
              break;
          case 0x7B:
              //STOP KEY
              if(flags.FLEETF == 0)
              {
                beep();
                REC_STOP = 1;
                __delay_ms(KEYDELAY);
                REC_STOP = 0;
                if(time.R == 1)
                {
                    rec_menu_level--;
                    if(rec_menu_level == 0)
                    {
                        //We've successfully backed out enough to no longer be in the rec menu
                        cleartime();
                        flags.CAMF = 1;
                    }
                }
                else
                {
                    cleartime();
                    flags.CAMF = 1;
                }
                    
                    //ReloadTimer(T1_VALUE);
                
              }
              else
              {
                  beep();
                  cleartime();
                  flags.CAMF = 1;
                  
              }
              break;
          case 0x77:
              //AUDIO ON/OFF KEY
              beep();
              toggleaudio();
              cleartime();
              break;
          case 0x7F:
              beep();
              //AUDIO VOLUME KEY
              PIE1bits.TMR1IE = 0;
              if(time.V == 0)
              {
                cleartime();
                time.V = 1;
                flags.CAMF = 0;
                ReloadTimer(T1_VALUE);
              }
              else
              {
                  cleartime();
                  flags.CAMF = 1;
                         
              }
              
              break;


      }
    }
}

void cleartime (void)
{
  //Stop Interrupt
  PIE1bits.TMR1IE = 0;
  PIR1bits.TMR1IF = 0;
  T1CONbits.TMR1ON = 0;
  
  if(time.R == 1)
  {
      //Make sure we're out of the record menu
      while(rec_menu_level > 0) //Back out of however many menu levels we are in.
      {
          reckey(_LEFT);
          __delay_ms(100);
      }

  }
  time.S = 0;
  time.R = 0;
  time.V = 0;
  time.B = 0;
  INT_DELAY = 0;
  

}

void togglepower (void)
{
    if(PWR == 1)    //Power was on
    {
        KILL = 0;
        MIRROR = 0;
        if(SHUT == 1)
        {
            SHUT = 0;
            __delay_ms(1500);
        }
        PWR = 0;
        OP_CS = 1;
        flags.PWRF = 0;
        DAC_SCK = 0;
        DAC_LOAD = 0;
        DAC_CS = 1;
        OP_CS = 1;
        flags.AUDIOF = 0;
        BEEP_SW = 0;
        AUDIO_SW = 0;
        MODE_STBY = 1;
        MODE_RUN = 0;
        WR_DAC(D_VALUE);

        flags.DNF = 0;
        
    }
    else
    {
        //Init System
        PWR = 1;
        OP_CS = 0;
        DAC_CS = 0;
        KILL = 1;
        DOWN = 1;
        flags.PWRF = 1;
        flags.CAMF = 1;
        
    }


}

void StartHeaterTimer (void)
{
        TMR0 = 0;
        OPTION_REGbits.T0CS = 0;
        OPTION_REGbits.PSA = 0;
        OPTION_REGbits.PS0 = 1;
        OPTION_REGbits.PS1 = 1;
        OPTION_REGbits.PS2 = 1;
        INTCONbits.TMR0IF = 0;
        
        INTCONbits.TMR0IE = 1;
}

void toggleaudio (void)
{
    if(flags.PWRF == 0)
        return;

    if(flags.AUDIOF == 1)
    {
        flags.AUDIOF = 0;
        BEEP_SW = 0;
        AUDIO_SW = 0;
        MODE_STBY = 1;
        MODE_RUN  = 0;
    }

    else if(flags.AUDIOF == 0)
    {
        flags.AUDIOF = 1;
        BEEP_SW = 0;
        AUDIO_SW = 1;
        MODE_STBY = 0;
        MODE_RUN = 1;

    }

   
}


void arrow (unsigned char direction)
{

    if(flags.CAMF == 1)
    {
        cam(direction);   //UP,LEFT,RIGHT,DOWN(BACK)
        return;
    }

    else if(time.S == 1)    //Switcher
    {
        serialsend(Message[direction]);
        return;
    }
    
    else if(time.R == 1)
    {
        reckey(direction);
        

        if(rec_menu_level == 0)
        {
            //We've successfully backed out enough to no longer be in the rec menu
            cleartime();
            flags.CAMF = 1;
        }
        //ReloadTimer(T1_VALUE);   
        return;
        
    }


    if((direction == _UP) || (direction == _DOWN))
    {

        if(time.B == 1)
        {
            brightness(direction);
        }
        else if(time.V == 1)
        {
            volume(direction);
        }

    }
    else
    {
       
        if(flags.CAMF == 0)
        {
            
            cleartime();
            flags.CAMF = 1;
            cam(direction);
        }
        
    }

}

void cam (unsigned char cam)
{

    if(cam == _FRONT)
        UP = !UP;
    else
        UP = 0;

    if(cam == _LEFT)
        LEFT = !LEFT;
    else
        LEFT = 0;

    if(cam == _RIGHT)
        RIGHT = !RIGHT;
    else
        RIGHT = 0;

    if(cam == _SPLIT)
        SPLIT = !SPLIT;
    else
        SPLIT = 0;

    if(cam == _BACK)
        DOWN = !DOWN;
    else
        DOWN = 0;
    //cleartime();
    //beep();
}

void reckey (unsigned char key)
{

    PIE1bits.TMR1IE = 0;
    REC_MENU = 0;
    RECORD = 0;
    REC_STOP = 0;
    REC_DOWN = 0;
    REC_UP = 0;

    if(key == _UP)
        REC_UP = 1;
    else if(key == _DOWN)
        REC_DOWN = 1;
    else if(key == _LEFT)
    {
        REC_STOP = 1;
        rec_menu_level--;
        
    }
    else if(key == _RIGHT)
    {
        REC_MENU = 1;
        rec_menu_level++;
    }

    __delay_ms(KEYDELAY);   //1.1 Second delay

    REC_UP = 0;
    REC_DOWN = 0;
    REC_STOP = 0;
    REC_MENU = 0;
    

}

void brightness(unsigned char direction)
{
    int temp;
    PIE1bits.TMR1IE = 0;

    temp = DAC_DATA;

    if(direction == _UP)
    {
        temp = temp + INC_VALUE;
        if(temp > 0xFF)
            temp = 0xFF;
    }
    else if(direction == _DOWN)
    {
        temp = temp - DEC_VALUE;
        if(temp < 0)
            temp = 0;
    }

    DAC_DATA = (char)temp;

    WR_DAC(DAC_DATA);

}

void volume (unsigned char direction)
{
    unsigned char pol;

    if(direction == _UP)
        pol = 1;
    else if(direction == _DOWN)
        pol = 0;

    VOL_CLK = pol;
    NOP();
    VOL_SW = 1;
    NOP();
    NOP();
    VOL_SW = 0;
    NOP();

    VOL_CLK = pol;
    NOP();
    VOL_SW = 1;
    NOP();
    NOP();
    VOL_SW = 0;
    NOP();

}

void beep (void)
{

    unsigned char x;

    BEEP_SW = 1;
    AUDIO_SW = 0;
    MODE_STBY = 0;
    MODE_RUN = 1;

    for(x=0xFF;x>0;x--)
    {
        __delay_ms(1);
        BEEP_SIG = !BEEP_SIG;
    }

    if(flags.AUDIOF == 0)
    {
        BEEP_SW = 0;
        AUDIO_SW = 0;
        MODE_STBY = 0;
        MODE_RUN = 1;
    }

    else
    {
        BEEP_SW = 0;
        AUDIO_SW = 1;
        MODE_STBY = 0;
        MODE_RUN = 1;
    }

}

void serialsend (unsigned long int data)
{
    //Sends data out LSB first
    //NEC Code
    //0 is 1.125ms long, 561.25 high, then 561 low
    //1 is 2.25ms long
    unsigned char x;
    PIE1bits.TMR1IE = 0;
    //Send Start Bit
    SERIAL_OUT = 1;
    __delay_ms(9);
    SERIAL_OUT = 0;
    __delay_us(4500);
    SERIAL_OUT = 1;

    //Send 32 bits now
    for(x=0;x<32;x++)
    {
        SERIAL_OUT = 1;
        __delay_us(541);
        SERIAL_OUT = 0;
        if((data & 0x01) == 1)
        {
            __delay_us(1676);
        }
        else
            __delay_us(541);

        data = data>>1; //Shift the next bit to be in position
    }

    //Time for the stop bit
    SERIAL_OUT = 1;
    __delay_us(541);
    SERIAL_OUT = 0;
}

void ReloadTimer (unsigned char length)
{
    T1COUNT = length;
    PIR1bits.TMR1IF = 0;
    TMR1L = 0;
    TMR1H = 0;
    T1CON = 0b00110101;
    PIE1bits.TMR1IE = 1;
    T1CONbits.TMR1ON = 1;
    //beep();
}
