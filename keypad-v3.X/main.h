/* 
 * File:   main.h
 * Author: filip
 *
 * Created on February 28, 2017, 4:05 PM
 */

#ifndef MAIN_H
#define	MAIN_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#define MSEC5 0x60
#define _XTAL_FREQ 4000000
//#define DELAY_100 __delay(100)    
    
    void init_system (void);
    void display (void);
    //void service (void);
    void timers (void);
    void scankeypad (void);
    unsigned char detect_key (unsigned char col);
    void id_key(void);
    void XMTR(void);
    void delay (unsigned char count);


#ifdef	__cplusplus
}
#endif

#endif	/* MAIN_H */

