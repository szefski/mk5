/*
 * File:   main.c
 * Author: filip
 *
 * Created on February 28, 2017, 3:58 PM
 */

#include <xc.h>
#include "main.h"

// CONFIG
#pragma config OSC = XT         // Oscillator selection bits (XT oscillator)
#pragma config WDT = OFF        // Watchdog timer enable bit (WDT disabled)
#pragma config CP = OFF         // Code protection bit (Code protection off)



struct {
unsigned KEY_FLAG :1,SERV_FLAG :1,XMT_FLAG :1,PWR_FLAG :1,DEBN_FLAG :1,SCAN_FLAG :1,SOUT_FLAG :1,SIN_FLAG :1; // TELLS SYSTEM A KEY PRESS HAPPENED.

} flags = {0,0,0,0,0,0,0,0};

//unsigned char LED_DATA_1 = 0;
//unsigned char LED_DATA_2 = 0;
//unsigned char LED_DATA_4 = 0;
//unsigned char LED_DATA_8 = 0;
//unsigned char LED_DATA_F = 0;

unsigned char LED_ROW = 0x01;

unsigned char KEY_DATA = 0;
unsigned char XMTREG = 0;
unsigned char KEYREG = 0;
/*
 *
 */
void main(void) {

    init_system();

    while(1)
    {
        display();
        //Service Code Here
        if(((flags.KEY_FLAG == 1) && (flags.SERV_FLAG == 0)))
         {
             flags.SERV_FLAG = 1;
             XMTREG = KEY_DATA;
             KEYREG = KEY_DATA;
             flags.XMT_FLAG = 1;
             id_key();

         }

         if(flags.SCAN_FLAG == 1)
         {
             scankeypad();
         }

         if(flags.SOUT_FLAG == 1)
         {
            XMTR();
         }
        //End of Service Code

        timers();

    }
}


void init_system (void)
{
    PORTA = 0x0F;
    PORTB = 0xFF;
    TRISA = 0x01;
    TRISB = 0xF0;

    OPTION = 0x04;
    flags.PWR_FLAG = 1; //We're on aren't we

    PORTAbits.RA3 = 0;
    PORTAbits.RA2 = 1;

    TMR0 = MSEC5;

}
void display (void)
{
    switch (LED_ROW){
        case 0x01:
            LED_ROW = 0x02;
            flags.SCAN_FLAG = 1;
            flags.SOUT_FLAG = 0;
            flags.SIN_FLAG = 0;
            break;
        case 0x02:
            LED_ROW = 0x04;
            flags.SCAN_FLAG = 0;
            flags.SOUT_FLAG = 1;
            flags.SIN_FLAG = 0;
            break;
        case 0x04:
            LED_ROW = 0x08;
            flags.SCAN_FLAG = 0;
            flags.SOUT_FLAG = 0;
            flags.SIN_FLAG = 1;
            break;
        case 0x08:
            LED_ROW = 0x00;
            flags.SCAN_FLAG = 0;
            flags.SOUT_FLAG = 0;
            flags.SIN_FLAG = 0;
            break;
        case 0x00:
            LED_ROW = 0x01;
            flags.SCAN_FLAG = 1;
            flags.SOUT_FLAG = 0;
            flags.SIN_FLAG = 0;
            break;

        default://Something messed up, lets reset the row to 0x01
            LED_ROW = 0x01;

    }

}


void timers (void)
{
    static unsigned char DEBOUNCE;
    
    while(TMR0);
    TMR0 = MSEC5;
    
    if(flags.KEY_FLAG ==1)
    {
        //Check for Debounce
        if(flags.DEBN_FLAG == 0)//This is first time we're running the debounce routine
        {
            flags.DEBN_FLAG = 1;
            DEBOUNCE = 0x90;
        }
        
        DEBOUNCE--;
        if(DEBOUNCE == 0)
        {
            flags.SERV_FLAG = 0;
            flags.KEY_FLAG = 0;
            flags.DEBN_FLAG = 0;
        }
        
    }
    //if Key flag is 0, exit
}

void scankeypad (void)
{

    flags.SCAN_FLAG = 0;
    if(flags.KEY_FLAG == 1)
        return;

    PORTAbits.RA2= 0;

    if(detect_key(0xFF))
        return;

    PORTAbits.RA2 = 1;

    if(detect_key(0b11110111))  //This tree is to quit looking for keys once we've found one
        return;
    if(detect_key(0b11111011))
        return;
    if(detect_key(0b11111101))
        return;
    if(detect_key(0b11111110))
        return;


}

unsigned char detect_key (unsigned char col)
{
    unsigned char portread;
    PORTB = 0xFF;
    NOP();
    PORTB = col;
    NOP();
    portread = PORTB;
    portread = (portread & 0xF0);
    //portread = portread ^ 0xF0;
    if(portread != 0xF0)
    {
        KEY_DATA = PORTB;
        flags.KEY_FLAG = 1;
        PORTAbits.RA2 = 1;
        PORTB = 0xFF;
        NOP();
        return 1;
    }
    return 0;

}

void id_key (void)
{

    switch(KEYREG)
    {
        case 0xDF:
            PORTAbits.RA3 = !PORTAbits.RA3;
            break;
        //case 0xD0:

        //    break;
        //case 0xB0:

        //    break;
        //case 0x70:

        //    break;
    }
}

void XMTR (void)
{
    //DX is 1
    //RX is 0
    unsigned char x;
    flags.SOUT_FLAG = 0;

    if(flags.XMT_FLAG == 0)
        return;
    //Send Start Bit
    PORTAbits.RA1 = 0;
    __delay_us(217);

    for(x=0;x<8;x++)
    {
        if((XMTREG & (0x01 << x)) == 0)
            PORTAbits.RA1 = 0;
        else
            PORTAbits.RA1 = 1;
        __delay_us(217);
    }

    PORTAbits.RA1 = 0;
    __delay_us(217);
    PORTAbits.RA1 = 1;
    if(PORTAbits.RA0 == 0)
    {
        flags.XMT_FLAG = 1;
        return;
    }
    __delay_us(217);

    if(PORTAbits.RA0 == 1)
    {
        flags.XMT_FLAG = 1;
        return;
    }

    flags.XMT_FLAG = 0;
    flags.SOUT_FLAG = 0;

}
