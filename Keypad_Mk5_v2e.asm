
; KEYPAD CONTROL PROGRAM BY DON LULHAM.
; COPYRIGHT 2013, LULHAM LOGIC INC.
; MAY 29, 2013.
;
; THIS VERSION IS WORKING. IT ECHOS THE KEY PRESSED TO THE DISPLAY.
; THE SERIAL OUT IS WORKING JUNE 10, 2013
; DECEMBER 9, 2013, ADDING SERIAL INPUT FROM VIDEO CONTROLLER. 4 LINES...
; DECEMBER 12, 2013, LAST WORKIMG CODE FOR V1 CONTROLLER WITHOUT SERIAL INPUT
; DECEMBER 17, 2013. ADDING ID KEY FOR LED CONTROL. SYSTEM WORKING...!
; DECEMBER 23, 2013, REMOVED SOFTWARE BLOCK SO HEATER LED COMES ON WHEN PWR IS OFF
; JANUARY 4, 2014, ADDING ACKNOWLEGDE FROM VIDEO CONTROLLER ON A TRANSMITION
; JANUARY 5, 2014. HANDSHAKE IS WORKING. UNIT IS SETUP FOR BACK CAMERA ON POWER UP
; JUNE 29, 2016, RECOVERING THIS FILE...
; JULY 6, 2016, FIXED ISSUE WITH KILL BUTTON
; OCTOBER 19, 2016, 20 KEY VERSION.
; OCTOBER 22, 2016, MODIFYING CODE FOR THE 20 BUTTONS
; OCTOBER 24, 2016, PROGRAMMING THE EXTRA 6 KEYS. PROBLEM WITH ROW-5 WITH THE LEDS...
; OCTOBER 25, 2016, GOT THE LEDS TO WORK CORRECTLY..SENDS CODE WHEN PWR IS OFF..!
; OCTOBER 25, 2016, WORKS WITH VCONTROL_MK4_TEST2.ASM
; NOVEMBER 1, 2016, ADDED BACK CAMERA ON POWER UP
; JANUARY 7, 2017,  CHANGED PROCESSOR TO THE 16F54. REMOVED LED STATUS LEDS..
; JANUARY 15, 2016, MODIFIED PROGRAM FOR 16F54. PROGRAMMED NEW KEYPAD. WORKS.!
;
; CHECKSUM = 0x193A
;------------------------------------------------------------------------------------
	TITLE "Keypad_MK5_v2e.asm"
	LIST P=P16F54
	#INCLUDE P16F54.INC
	__config _XT_OSC & _WDT_OFF & _CP_OFF

;------------------------------------------------------------------------------------
;DEFINE EQUATES:
;------------------------------------------------------------------------------------
C 			EQU 	0 			; OR 	XOR 	AND
DC 			EQU	 	1 			; ------ ------ ------
Z 			EQU 	2 			; 00 / 0 00 / 0 00 / 0
PA1 		EQU 	6 			; 01 / 1 01 / 1 01 / 0
PA0 		EQU 	5 			; 10 / 1 10 / 1 10 / 0
F0 			EQU 	0 			; 11 / 1 11 / 0 11 / 1
CARRY 		EQU 	0
F 			EQU 	1
MSEC5 		EQU 	0x60 		; 5.2 MILLISECONDS
DX 			EQU 	1 			; OUTPUT BIT FOR SERIAL TRANSMITION
RX 			EQU 	0 			; INPUT BIT FOR SERIAL RECEPTION
MSB 		EQU 	7
KEY_FLAG 	EQU 	0 			; TELLS SYSTEM A KEY PRESS HAPPENED.
SERV_FLAG 	EQU 	1 			; TELLS SERVICE TO GET DATA FROM NEW_KEY INTO XMTREG
XMT_FLAG 	EQU 	2 			; TELLS SYSTEM THAT THERE IS DATA IN THE XMTREG
PWR_FLAG 	EQU 	3 			; 0 = on 1 = off..?
DEBN_FLAG 	EQU 	4 			; TELLS DEBOUNCE THAT THE COUNTER IS LOADED
SCAN_FLAG 	EQU 	5 			; TELLS SERVICE TO DO A SCAN_KP
SOUT_FLAG 	EQU 	6 			; TELLS SERVICE TO DO A SERIAL OUT
SIN_FLAG 	EQU 	7 			; TELLS SERVICE TO DO A SERIAL IN
A3			EQU		3			; PORT_A, BIT 3, USED BY HEATER STATUS LED
A2			EQU		2			; PORT_A, BIT 2. USED BY KEYPAD SCAN

;------------------------------------------------------------------------------------
;DEFINE RAM LOCATIONS:
;------------------------------------------------------------------------------------
RTCC 		EQU 	1
PC 			EQU 	2
STATUS 		EQU 	3
FSR 		EQU 	4
PORT_A 		EQU 	5
PORT_B 		EQU 	6
DCOUNT 		EQU 	9 			; COUNTER USED BY DELAY100
FLAGS 		EQU 	0x0A 		; MAIN CONTROL FLAGS
TEMP 		EQU 	0x0B
KEY_DATA 	EQU 	0x0C 		; NEW KEYPAD DATA SAVED FROM KEYSCAN
DEBOUNCE 	EQU 	0x0D 		; DEBOUNCE COUNTER
XMTREG 		EQU 	0x0E 		; HOLDS KEYPAD DATA FOR SERIAL OUTPUT
RCVREG 		EQU 	0x0F 		; HOLDS DATA RECIEVED BY RCVR SUBROUTINE
KEYREG		EQU		0x19		;
XCOUNT 		EQU 	10 			; HOLDS NUMBER OF BITS BEING TRANSMITTED
RCOUNT 		EQU 	11 			; HOLDS NUMBER OF BITS BEING RECIEVED
LED_DATA_X 	EQU 	12 			; ANODE DATA FROM LED_DATA_X, USED BY ID KEY ROUTINE.
LED_DATA_1 	EQU 	13 			; COLUMN DATA FOR LED ANODES 1,2,3,4
LED_DATA_2 	EQU 	14 			; COLUMN DATA FOR LED ANODES 5,6,7,8
LED_DATA_4 	EQU 	15 			; COLUMN DATA FOR LED ANODES 9,10,11,12
LED_DATA_8 	EQU 	16 			; COLUMN DATA FOR LED ANODES 13,14,15,16
LED_DATA_F	EQU		17			; COLUMN DATA FOR LED ANODES 17,18,19,20
LED_ROW 	EQU 	18 			; ROW SELECT DATA FOR LED CATHODES

;------------------------------------------------------------------------------------
;PORT PIN DEFINITIONS:
;------------------------------------------------------------------------------------
;PORT-A 
; A0 = INPUT  = SERIAL IN
; A1 = OUTPUT = SERIAL OUT
; A2 = OUTPUT = KEYPAD COLUMN-5
; A3 = OUTPUT = HEATER STATUS LED
; TOCK1 = INPUT = TIED TO GND

;PORT-B
; B0 = OUTPUT = KEYPAD COLUMN-1
; B1 = OUTPUT = KEYPAD COLUMN-2 
; B2 = OUTPUT = KEYPAD COLUMN-3
; B3 = OUTPUT = KEYPAD COLUMN-4
; B4 = OUTPUT = KEYPAD ROW-1
; B5 = OUTPUT = KEYPAD ROW-2
; B6 = OUTPUT = KEYPAD ROW-3 - CLK
; B7 = OUTPUT = KEYPAD ROW-4 - DAT
;------------------------------------------------------------------------------------

	ORG 	0x0000
START
	GOTO 	INITS_SYSTEM 		;INITIALIZE CLOCK

;------------------------------------------------------------------------------------
; MAIN_LOOP ROUTINE PAGE-0
;------------------------------------------------------------------------------------
MAIN_LOOP 						; THIS IS THE MAIN PROGRAM LOOP...!
	CALL 	DISPLAY 			; DISPLAY 
	NOP
	CALL 	SERVICE 			; SERVICE 
	NOP
	CALL 	TIMERS 				; TIMERS 
	NOP
	GOTO 	MAIN_LOOP 			; LOOP BACK AND DO IT AGAIN.....!

;------------------------------------------------------------------------------------
; DISPLAY SUBROUTINE -DETECTS WHICH STATE THE CODE IS IN. UPDATES LEDS PAGE-0
;------------------------------------------------------------------------------------
DISPLAY
	NOP
UPDATE_1
	MOVLW 	0x01
	SUBWF 	LED_ROW,W 			; IS THIS ROW 1..?
	BTFSC 	STATUS,Z
	GOTO 	LED_ROW_1 			; YES = 1

	MOVLW 	0x02
	SUBWF 	LED_ROW,W 			; IS THIS ROW 2..?
	BTFSC 	STATUS,Z
	GOTO 	LED_ROW_2 			; YES = 2

	MOVLW 	0x04
	SUBWF 	LED_ROW,W 			; IS THIS ROW 4..?
	BTFSC 	STATUS,Z
	GOTO 	LED_ROW_4 			; YES = 4

	MOVLW 	0x08
	SUBWF 	LED_ROW,W 			; IS THIS ROW 8..?
	BTFSC 	STATUS,Z
	GOTO 	LED_ROW_8 			; YES = 8

	MOVLW 	0x00
	SUBWF 	LED_ROW,W 			; IS THIS ROW F..?
	BTFSC 	STATUS,Z
	GOTO 	LED_ROW_F 			; YES = F

	MOVLW 	0x01 				; RESET LED_ROW TO 1.
	MOVWF 	LED_ROW 			; THERE MUST HAVE BEEN AN ERROR..!
	GOTO 	UPDATE_1 			; SO TRY AGAIN

LED_ROW_1					
	MOVLW 	0x02
	MOVWF 	LED_ROW 			; INC LED_ROW NUMBER
	BSF 	FLAGS,SCAN_FLAG 	; SET FLAG FOR KEYPAD SCAN
	BCF 	FLAGS,SOUT_FLAG
	BCF 	FLAGS,SIN_FLAG
	RETLW 	0

LED_ROW_2 							
	MOVLW 	0x04
	MOVWF 	LED_ROW 			; INC LED_ROW NUMBER
	BSF 	FLAGS,SOUT_FLAG 	; SET FLAG FOR SERIAL OUTPUT.
	BCF 	FLAGS,SCAN_FLAG
	BCF 	FLAGS,SIN_FLAG
	RETLW 	0

LED_ROW_4 							
	MOVLW 	0x08
	MOVWF 	LED_ROW 			; INC LED_ROW NUMBER
	BSF 	FLAGS,SIN_FLAG 		; SET FLAG FOR SERIAL INPUT.
	BCF 	FLAGS,SCAN_FLAG
	BCF 	FLAGS,SOUT_FLAG
	RETLW 	0

LED_ROW_8 							
	MOVLW 	0x00	
	MOVWF 	LED_ROW 			; INC LED_ROW NUMBER
	BCF 	FLAGS,SIN_FLAG
	BCF 	FLAGS,SCAN_FLAG
	BCF 	FLAGS,SOUT_FLAG
	RETLW 	0

LED_ROW_F
	MOVLW 	0x01	
	MOVWF 	LED_ROW 			; RESET LED_ROW NUMBER
	BCF 	FLAGS,SIN_FLAG
	BCF 	FLAGS,SCAN_FLAG
	BCF 	FLAGS,SOUT_FLAG
	RETLW 	0

;------------------------------------------------------------------------------------
; INITS_SYSTEM ROUTINE ; PAGE-0
;------------------------------------------------------------------------------------
INITS_SYSTEM
	MOVLW 	B'00001111' 		; MAKE ACTIVE HIGH
	MOVWF 	PORT_A

	MOVLW 	B'00000001' 		; SET PORT A AS INPUT/OUTPUTS
	TRIS	PORT_A				;

	MOVLW 	B'11111111' 		; SET LEVELS HIGH
	MOVWF 	PORT_B

	MOVLW 	B'11110000' 		; SET PORT B AS INPUTS AND OUTPUTS
	TRIS	PORT_B				;

	MOVLW 	B'00000100'
	OPTION

	MOVLW 	MSEC5 				; RTCC = 5 mSEC
	MOVWF 	RTCC 				; LOAD RTCC TIMER

	CLRF 	DEBOUNCE 			; CLEAR DEBOUNCE TIMER
	CLRF 	FLAGS
	BSF		FLAGS,PWR_FLAG		; 1= PWR OFF 0= PWR ON
	
	MOVLW 	B'00000000' 		; CLEAR LED TO OFF
	MOVWF 	LED_DATA_1
	MOVWF 	LED_DATA_2
	MOVWF 	LED_DATA_4
	MOVWF 	LED_DATA_8
	MOVWF	LED_DATA_F

	BCF		PORT_A,A3			; ACTIVE HIGH SO MAKE LOW
	NOP
	BSF		PORT_A,A2			; ACTIVE LOW SO MAKE HIGH
	NOP

	MOVLW 	0x01
	MOVWF 	LED_ROW 			; START WITH LED_ROW_1
	GOTO 	MAIN_LOOP

;------------------------------------------------------------------------------------
; TIMERS SUBROUTINE ; PAGE-1
;------------------------------------------------------------------------------------
TIMERS
	MOVF 	RTCC,W 				; SEE IF RTCC = 0
	BTFSS 	STATUS,Z 			; IF 0 THEN SKIP
	GOTO 	TIMERS 				; ELSE LOOP WAIT FOR OVERFLOW TO 0
	MOVLW 	MSEC5 				; RTCC = 5 mSEC
	MOVWF 	RTCC
	BTFSC 	FLAGS,KEY_FLAG 		; NO KEY HIT THEN SKIP
	GOTO 	CHK_DEBOUNCE 		; ELSE DEBOUNCE
	RETLW 	0

CHK_DEBOUNCE
	BTFSC 	FLAGS,DEBN_FLAG 	; DEBOUNCE RUNNING..?
	GOTO 	DO_DEBOUNCE 		; YES SO LEAVE COUNTER AALONE
	BSF 	FLAGS,DEBN_FLAG 	; SET FLAG
	MOVLW 	0x90 				; ELSE DB 100 mSEC
	MOVWF 	DEBOUNCE

DO_DEBOUNCE
	DECFSZ 	DEBOUNCE, F 		; DEC AND CHK
	RETLW 	0
	BCF 	FLAGS,SERV_FLAG 	; ELSE CLEAR FLAGS
	BCF 	FLAGS,KEY_FLAG
	BCF 	FLAGS,DEBN_FLAG
	RETLW 	0

;------------------------------------------------------------------------------------
; SERVICE SUBROUTINE ; PAGE-2
;------------------------------------------------------------------------------------
SERVICE
	BTFSS 	FLAGS,KEY_FLAG 		; NO KEY HIT THEN ...
	GOTO 	TESTFLAGS

	BTFSC 	FLAGS,SERV_FLAG 	; IF NOT SERVICED SKIP
	GOTO 	TESTFLAGS
								
	BSF 	FLAGS,SERV_FLAG 	; SET SERVICED FLAG
	MOVFW 	KEY_DATA
	MOVWF 	XMTREG 				; SAVE KEYPAD DATA
	MOVWF	KEYREG				; THIS IS FOR ID_KEY ROUTINE
	BSF 	FLAGS,XMT_FLAG 		; SET FLAG
	GOTO	ID_KEY

TESTFLAGS
	BTFSC 	FLAGS,SCAN_FLAG 	; TEST FOR KEYPAD SCAN
	GOTO 	SCAN_KP				; JUMP TO SCAN_KP IF SET

K_RETURN
	BTFSC 	FLAGS,SOUT_FLAG 	; TEST FOR SERIAL OUT TO VIDEO CONTROLLER
	GOTO 	XMTR				; JUMP TO XMTR IF SET

X_RETURN
	NOP
	RETLW	0

;------------------------------------------------------------------------------------
; DELAY100 SUBROUTINE
;------------------------------------------------------------------------------------
DELAY100
	MOVLW	50			
	MOVWF	DCOUNT
DLOOP
	DECFSZ	DCOUNT,F
	GOTO	DLOOP
	RETLW	0

;------------------------------------------------------------------------------------
; SCAN_KP ROUTINE ; PAGE-2
;------------------------------------------------------------------------------------
SCAN_KP
	BCF 	FLAGS,SCAN_FLAG
	BTFSC 	FLAGS,KEY_FLAG 		; KEY UNDER SERVICE?
	GOTO 	K_RETURN 			; RETURN

; CHECK A2 COLUMN FIRST..

	BCF		PORT_A,A2			; CLEAR A2, COL-5
	MOVLW 	B'11111111' 		; SET ALL HIGH
	MOVWF 	PORT_B 				;
	nop
	MOVF 	PORT_B,W 			; INPUT PORT VALUE
	ANDLW 	B'11110000' 		; MASK LO BYTE
	XORLW 	B'11110000' 		; SEE IF KEY HIT
	BTFSS 	STATUS,Z 			; NO KEY THEN SKIP
	GOTO 	DET_KEY 			; LOAD KEY VALUE

; CHECK THE OTHER 4 COLUMNS....

	BSF		PORT_A,A2			; TURN OFF COL-5
	MOVLW 	B'11111111' 		; SET ALL HIGH
	MOVWF 	PORT_B 				;
	MOVLW 	B'11110111' 		; SET KEY COL LOW
	MOVWF 	TEMP 				; SAVE IN TEMP
MORECOL
	MOVF 	TEMP,W 				; GET OLD VALUE
	MOVWF 	PORT_B 				; OUTPUT TO PORT
	NOP
	MOVF 	PORT_B,W 			; INPUT PORT VALUE
	ANDLW 	B'11110000' 		; MASK LO BYTE
	XORLW 	B'11110000' 		; SEE IF KEY HIT
	BTFSS 	STATUS,Z 			; NO KEY THEN SKIP
	GOTO 	DET_KEY 			; LOAD KEY VALUE

	BSF 	STATUS,C 			; SET CARRY
	RRF 	TEMP, F 			; MAKE NEXT COL. LOW
	BTFSC 	STATUS,C 			; ALL DONE THEN SKIP
	GOTO 	MORECOL
SKP1
	GOTO 	K_RETURN 			; RETURN

DET_KEY 						; key is detected
	MOVF 	PORT_B,W 			; GET RAW KEY...
	MOVWF 	KEY_DATA 			; SAVE IN NEW_KEY RAW HEX
	BSF 	FLAGS,KEY_FLAG 		; SET KEY HIT FLAG
	BSF		PORT_A,A2
	GOTO 	SKP1 				; RETURN

;------------------------------------------------------------------------------------
; XMTR ROUTINE ; PAGE-2
;------------------------------------------------------------------------------------
XMTR
	BCF 	FLAGS,SOUT_FLAG
	BTFSS 	FLAGS,XMT_FLAG
	GOTO 	X_RETURN

	MOVLW 	8
	MOVWF 	XCOUNT
	BCF 	PORT_A,DX 			; SEND START BIT
	CALL 	DELAY100
X_NEXT
	BCF 	STATUS,CARRY
	RRF 	XMTREG,F
	BTFSC 	STATUS,CARRY
	BSF 	PORT_A,DX
	BTFSS 	STATUS,CARRY
	BCF 	PORT_A,DX
	CALL 	DELAY100
	DECFSZ 	XCOUNT,F
	GOTO 	X_NEXT

	BCF 	PORT_A,DX 			; SEND STOP BIT
	CALL 	DELAY100
	BSF 	PORT_A,DX

; STOP BIT HAS BEEN SENT. LOOK FOR AN ACK SIGNAL ON RX

	BTFSS	PORT_A,RX
	GOTO	BADDX
	CALL	DELAY100
	BTFSC	PORT_A,RX
	GOTO	BADDX

	BCF 	FLAGS,XMT_FLAG
	BCF 	FLAGS,SOUT_FLAG
	GOTO 	X_RETURN

BADDX
	BSF		FLAGS,XMT_FLAG
	GOTO	X_RETURN

;------------------------------------------------------------------------------------
; ID_KEY ROUTINE
;------------------------------------------------------------------------------------
ID_KEY
	MOVFW	KEYREG
	ANDLW	B'11110000'
	MOVWF	LED_DATA_X

	MOVLW	0xE0
	SUBWF	LED_DATA_X,W
	BTFSC	STATUS,Z
	GOTO	KEYROW_1			; KEYPAD ROW 1

	MOVLW	0xD0
	SUBWF	LED_DATA_X,W
	BTFSC	STATUS,Z
	GOTO	KEYROW_2			; KEYPAD ROW 2

	MOVLW	0xB0
	SUBWF	LED_DATA_X,W
	BTFSC	STATUS,Z
	GOTO	KEYROW_3			; KEYPAD ROW 3

	MOVLW	0x70
	SUBWF	LED_DATA_X,W
	BTFSC	STATUS,Z
	GOTO	KEYROW_4			; KEYPAD ROW 4

	GOTO	TESTFLAGS			; ERROR....!

KEYROW_1						; KEYS STARTING WITH "E"
	MOVFW	KEYREG
	ANDLW	B'00001111'
	MOVWF	TEMP

	MOVLW	0x0E
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_EE				; POWER ON/OFF BUTTON-1

	MOVLW	0x0D
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_ED				; MIRROR ON/OFF BUTTON-2

	MOVLW	0x0B
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_EB				; KILL BUTTON-3

	MOVLW	0x07
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_E7				; SHUTTERS BUTTON-4

	MOVLW	0x0F
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_EF				; UP ARROW BUTTON-5

	GOTO	TESTFLAGS

KEYROW_2						; KEYS STARTING WITH "D"
	MOVFW	KEYREG
	ANDLW	B'00001111'
	MOVWF	TEMP

	MOVLW	0x0E
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_DE				; N/D BUTTON-6

	MOVLW	0x0D
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_DD				; LEFT ARROW BUTTON-7

	MOVLW	0x0B
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_DB				; SPLIT BUTTON-8

	MOVLW	0x07
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_D7				; RIGHT ARROW BUTTON-9

	MOVLW	0x0F
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_DF				; HEATERS BUTTON-10

	GOTO	TESTFLAGS

KEYROW_3						; KEYS STARTING WITH "B"
	MOVFW	KEYREG
	ANDLW	B'00001111'
	MOVWF	TEMP

	MOVLW	0x0E
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_BE				; DOWN ARROW BUTTON-11

	MOVLW	0x0D
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_BD				; SWITCHER MENU BUTTON-12

	MOVLW	0x0B
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_BB				; AV BUTTON-13

	MOVLW	0x07
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_B7				; QUAD BUTTON-14

	MOVLW	0x0F
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_BF				; SWITCHER SEL/JUMP BUTTON-15

	GOTO	TESTFLAGS

KEYROW_4						; KEYS STARTING WITH "7"
	MOVFW	KEYREG
	ANDLW	B'00001111'
	MOVWF	TEMP

	MOVLW	0x0E
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_7E				; RECORDER MENU BUTTON-16

	MOVLW	0x0D
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_7D				; RECORD BUTTON-17

	MOVLW	0x0B
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_7B				; STOP BUTTON-18

	MOVLW	0x07
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_77				; AUDIO ON/OFF BUTTON-19

	MOVLW	0x0F
	SUBWF	TEMP,W
	BTFSC	STATUS,Z
	GOTO	KEY_7F				; AUDIO VOLUME BUTTON-20

	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; PWR_KEY - TOGGLE ROUTINE - BUTTON-1, LED-1 - CODE EE
;------------------------------------------------------------------------------------
KEY_EE
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; MIRROR_KEY - TOGGLE ROUTINE - BUTTON-2, LED-2, - CODE ED
;------------------------------------------------------------------------------------
KEY_ED
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; KILL KEY - TOGGLE ROUTINE - BUTTON-3, LED-3, CODE EB
;------------------------------------------------------------------------------------
KEY_EB
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; SHUT KEY - TOGGLE ROUTINE - BUTTON-4, LED-4, CODE E7
;------------------------------------------------------------------------------------
KEY_E7
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; UP ARROW KEY CAM-1 (FRONT)- BUTTON-5, LED-5 - CODE EF
;------------------------------------------------------------------------------------
KEY_EF
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; BRIGHTNESS KEY - TOGGLE ROUTINE - BUTTON-6, LED-6, - CODE DE
;------------------------------------------------------------------------------------
KEY_DE
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; LEFT ARROW KEY CAM-2 (LEFT) - BUTTON-7, LED-7, CODE DD
;------------------------------------------------------------------------------------
KEY_DD
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; SPLIT KEY - BUTTON-8, LED-8, CODE DB
;------------------------------------------------------------------------------------
KEY_DB
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; RIGHT ARROW KEY CAM-3 (RIGHT) - BUTTON-9, LED-9, CODE D7
;------------------------------------------------------------------------------------
KEY_D7
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; HEAT KEY - TOGGLE ROUTINE - BUTTON-10, LED-10, CODE DF
;------------------------------------------------------------------------------------
KEY_DF
	BTFSS	PORT_A,A3
	GOTO	HEAT_OFF
	BCF		PORT_A,A3
	GOTO	TESTFLAGS

HEAT_OFF
	BSF		PORT_A,A3
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; DOWN ARROW KEY CAM-4 (BACK)- BUTTON-11, LED-11, CODE BE
;------------------------------------------------------------------------------------
KEY_BE
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; SWITCHER MENU - BUTTON-12, LED-12, CODE BD
;------------------------------------------------------------------------------------
KEY_BD
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; AV - BUTTON-13, LED-13, CODE BB
;------------------------------------------------------------------------------------
KEY_BB
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; QUAD - BUTTON-14, LED-14, CODE B7
;------------------------------------------------------------------------------------
KEY_B7
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; SWITCHER SEL/JUMP - BUTTON-15, LED-15, CODE BF
;------------------------------------------------------------------------------------
KEY_BF
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; RECORDER MENU - BUTTON-16, LED-16, CODE 7E
;------------------------------------------------------------------------------------
KEY_7E
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; RECORD - BUTTON-17, LED-17, CODE 7D
;------------------------------------------------------------------------------------
KEY_7D
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; REC STOP - BUTTON-18, LED-18, CODE 7B
;------------------------------------------------------------------------------------
KEY_7B
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; AUDIO ON/OFF - TOGGLE ROUTINE - BUTTON-19, LED-19, CODE 77
;------------------------------------------------------------------------------------
KEY_77
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
; AUDIO VOLUME - BUTTON-20, LED-20, CODE 7F
;------------------------------------------------------------------------------------
KEY_7F
	GOTO	TESTFLAGS

;------------------------------------------------------------------------------------
	ORG 	0x01FF
	NOP

;------------------------------------------------------------------------------------
	END

;------------------------------------------------------------------------------------
;KEYPAD ARRAY ADDRESSES:
;------------------------------------------------------------------------------------
; 		C0 		C1 		C2 		C3 		A3
;--------------------------------------------
;C4 	KEY-1 	KEY-2 	KEY-3 	KEY-4	KEY-5
; 		EEH 	EDH 	EBH 	E7H		EFH
;
;C5 	KEY-6 	KEY-7 	KEY-8	KEY-9	KEY-10
; 		DEH 	DDH 	DBH 	D7H		DFH	
;
;C6 	KEY-11 	KEY-12 	KEY-13 	KEY-14	KEY-15
; 		BEH 	BDH 	BBH 	B7H		BFH
;
;C7 	KEY-16 	KEY-17 	KEY-18 	KEY-19	KEY-20
; 		7EH 	7DH 	7BH 	77H		7FH
;
;------------------------------------------------------------------------------------
;	KEY		FUNCTION		ADDRESS		LED		ADDRESS
;	#						PORT-C		#		PORT-B
;------------------------------------------------------------------------------------
;
;	B1	=	POWER ON/OFF		EE		D1 		11
;	B2  =	MIRROR OM/OFF		ED		D2		21
;	B3  =	KILL				EB		D3		43
;	B4  =	SHUTTERS			E7		D4		81
;	B5  =	UP ARROW			EF		D5		12
;	B6  =	BRITNESS			DE		D6		22
;	B7  =	LEFT ARROW			DD		D7		42
;	B8  =	SPLIT				DB		D8		82
;	B9  =	RIGHT ARROW			D7		D9		14
;	B10 =	HEATERS				DF		D10		24
;	B11 =	DOWN ARROW			BE		D11		44
;	B12 =	SWITCHER MENU		BD		D12		84
;	B13 =	AV					BB		D13		18
;	B14 =	QUAD				B7		D14		28
;	B15 =	SELECT/JUMP			BF		D15		48
;	B16 =	RECORDER MENU		7E		D16		88	
;	B17 =	RECORD/PAUSE		7D		D17		10
;	B18 =	STOP/BACK			7B		D18		20
;	B19 =	AUDIO ON/OFFF		77		D19		40
;	B20 =	AUDIO VOLUME		7F		D20		80
;
;----------------------------------------------------------------------------------